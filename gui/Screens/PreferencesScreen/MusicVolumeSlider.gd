extends HSlider

func _on_value_changed(value):
	Rakugo.store.set("music_volume", value)

func _on_visibility_changed():
	if visible:
		var result = Rakugo.store.get("music_volume")
		if result:
			value = result
		else:
			value = 0

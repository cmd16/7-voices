extends HSlider

func _on_value_changed(value):
	Rakugo.store.set("voice_volume", value)

func _on_visibility_changed():
	if visible:
		var result = Rakugo.store.get("voice_volume")
		if result:
			value = result
		else:
			value = 0

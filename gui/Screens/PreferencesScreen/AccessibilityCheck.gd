extends CheckButton

func _on_visibility_changed():
	var result = Settings.get("accessibility")
	if result == null:
		result = true
	self.pressed = result

func _on_AccessibilityCheck_toggled(button_pressed):
	Settings.set("accessibility", button_pressed)

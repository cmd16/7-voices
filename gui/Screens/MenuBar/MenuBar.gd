extends Panel

signal show_menu(menu, game_started)
signal nav_button_press(nav_action)

func _ready():
	connect_buttons()
	disable_continue_button()
	_show_menu("main_menu", Rakugo.started)

func disable_continue_button():
	if not File.new().file_exists(Rakugo.StoreManager.get_save_path("auto")):
		for n in get_tree().get_nodes_in_group("nav_button_continue"):
			n.disabled = true

func _show_menu(menu, game_started):
	emit_signal("show_menu", menu, game_started)

func connect_buttons():
	for nb in get_tree().get_nodes_in_group("nav_button"):
		nb.connect("nav_button_pressed", self, "_on_nav_button_pressed")
	
func _on_nav_button_pressed(nav_action):
	if nav_action != "quit":
		_show_menu(nav_action, Rakugo.started)

	emit_signal("nav_button_press", nav_action)


func _on_Start_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("start")


func _on_Continue_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("continue")


func _on_History_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("history")


func _on_Save_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("save")


func _on_Load_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("load")


func _on_Preferences_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("preferences")


func _on_MainMenu_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("main_menu")


func _on_About_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("about")


func _on_Help_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("help")


func _on_Quit_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("quit")


func _on_ReturnButton_mouse_entered():
	if Settings.get("accessibility"):
		ChoicePlayer.play_sound("return")

extends Control


signal quick_button_press(quick_action)

func _ready():
	connect_buttons()

func connect_buttons():
	for qb in get_tree().get_nodes_in_group("quick_button"):
		qb.connect("quick_button_pressed", self, "_on_quick_button_pressed")


func _on_quick_button_pressed(quick_action):
	emit_signal("quick_button_press", quick_action)


func _on_History_mouse_entered():
	ChoicePlayer.play_sound("history")


func _on_Save_mouse_entered():
	ChoicePlayer.play_sound("save")


func _on_Load_mouse_entered():
	ChoicePlayer.play_sound("load")


func _on_Skip_mouse_entered():
	ChoicePlayer.play_sound("skip")


func _on_Preferences_mouse_entered():
	ChoicePlayer.play_sound("preferences")


func _on_Quit_mouse_entered():
	ChoicePlayer.play_sound("quit")

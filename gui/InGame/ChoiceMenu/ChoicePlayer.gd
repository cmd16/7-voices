extends AudioStreamPlayer

const sounds = {
	"Bard": preload("res://assets/audio/va/accessibility/bard.wav"),
	"Cleric": preload("res://assets/audio/va/accessibility/cleric.wav"),
	"Druid": preload("res://assets/audio/va/accessibility/druid.wav"),
	"Paladin": preload("res://assets/audio/va/accessibility/paladin.wav"),
	"Rogue": preload("res://assets/audio/va/accessibility/rogue.wav"),
	"Sorcerer": preload("res://assets/audio/va/accessibility/sorcerer.wav"),
	"Wizard": preload("res://assets/audio/va/accessibility/wizard.wav"),
	"1": preload("res://assets/audio/va/accessibility/1.wav"),
	"2": preload("res://assets/audio/va/accessibility/2.wav"),
	"3": preload("res://assets/audio/va/accessibility/3.wav"),
	"About the amulet": preload("res://assets/audio/va/accessibility/about_amulet.wav"),
	"About your whereabouts": preload("res://assets/audio/va/accessibility/about_where.wav"),
	"About you": preload("res://assets/audio/va/accessibility/about_you.wav"),
	"Make my accusation": preload("res://assets/audio/va/accessibility/accusation.wav"),
	"Yes, make an accusation": preload("res://assets/audio/va/accessibility/accusation_confirm.wav"),
	"No, go back to talking to characters": preload("res://assets/audio/va/accessibility/accusation_return.wav"),
	"What do you know about the amulet?": preload("res://assets/audio/va/accessibility/amulet_know.wav"),
	"What would you do if you had the amulet?": preload("res://assets/audio/va/accessibility/amulet_motive.wav"),
	"What was it like when the amulet was discovered?": preload("res://assets/audio/va/accessibility/amulet_discovered.wav"),
	"Done": preload("res://assets/audio/va/accessibility/done.wav"),
	"Never mind.": preload("res://assets/audio/va/accessibility/never_mind.wav"),
	"No more questions.": preload("res://assets/audio/va/accessibility/no_more_questions.wav"),
	"Why are you with this group?": preload("res://assets/audio/va/accessibility/you_group.wav"),
	"What are you good at?": preload("res://assets/audio/va/accessibility/you_skills.wav"),
	"Stay with the group": preload("res://assets/audio/va/accessibility/stay_option.wav"),
	"Leave the group": preload("res://assets/audio/va/accessibility/leave_option.wav"),
	"history": preload("res://assets/audio/va/accessibility/history.wav"),
	"load": preload("res://assets/audio/va/accessibility/load.wav"),
	"skip": preload("res://assets/audio/va/accessibility/skip.wav"),
	"preferences": preload("res://assets/audio/va/accessibility/preferences.wav"),
	"save": preload("res://assets/audio/va/accessibility/save.wav"),
	"quit": preload("res://assets/audio/va/accessibility/quit.wav"),
	"confirm_quit": preload("res://assets/audio/va/accessibility/confirm_quit.wav"),
	"Where were you when the amulet was discovered?": preload("res://assets/audio/va/accessibility/where_amulet.wav"),
	"Where were you during the party?": preload("res://assets/audio/va/accessibility/where_party.wav"),
	"Where were you after the party?": preload("res://assets/audio/va/accessibility/where_night.wav"),
	"return": preload("res://assets/audio/va/accessibility/return.wav"),
	"main_menu": preload("res://assets/audio/va/accessibility/main_menu.wav"),
	"voice_volume": preload("res://assets/audio/va/accessibility/volume_voice.wav"),
	"music_volume": preload("res://assets/audio/va/accessibility/volume_music.wav"),
	"master_volume": preload("res://assets/audio/va/accessibility/master_volume.wav"),
	"voice_volume_changed": preload("res://assets/audio/va/accessibility/volume_voice_changed.wav"),
	"help": preload("res://assets/audio/va/accessibility/help.wav"),
	"credits": preload("res://assets/audio/va/accessibility/credits.wav"),
	"continue": preload("res://assets/audio/va/accessibility/continue.wav"),
	"start": preload("res://assets/audio/va/accessibility/start.wav"),
	"about": preload("res://assets/audio/va/accessibility/about.wav"),
}

func play_sound(sound_name):
	print(self.name, " playing sound: ", sound_name)
	if !self.sounds.has(sound_name):
		print(self.name, " skipping sound: ", sound_name)
		return
	self.stream = self.sounds[sound_name]
	self.play()

func play_sounds(sounds):
	for sound in sounds:
		self.play_sound(sound)
		yield(self, "finished")

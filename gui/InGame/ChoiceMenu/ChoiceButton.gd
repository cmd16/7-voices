extends Button

signal choice_button_pressed(button)
signal choice_button_mouseover(button)

func _on_pressed():
	self.emit_signal("choice_button_pressed", self)

func _on_ChoiceButton_mouse_entered():
	self.emit_signal("choice_button_mouseover", self)

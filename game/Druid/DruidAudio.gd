extends AudioStreamPlayer

const sounds = {
	"druid_amulet": preload("res://assets/audio/va/druid_amulet.wav"),
	"druid_amulet_discovered_0": preload("res://assets/audio/va/druid_amulet_discovered_0.wav"),
	"druid_amulet_discovered_1": preload("res://assets/audio/va/druid_amulet_discovered_1.wav"),
	"druid_amulet_discovered_2": preload("res://assets/audio/va/druid_amulet_discovered_2.wav"),
	"druid_amulet_know_0": preload("res://assets/audio/va/druid_amulet_know_0.wav"),
	"druid_amulet_know_1": preload("res://assets/audio/va/druid_amulet_know_1.wav"),
	"druid_amulet_know_insight_0": preload("res://assets/audio/va/druid_amulet_know_insight_0.wav"),
	"druid_amulet_know_persuasion_0": preload("res://assets/audio/va/druid_amulet_know_persuasion_0.wav"),
	"druid_amulet_know_persuasion_1": preload("res://assets/audio/va/druid_amulet_know_persuasion_1.wav"),
	"druid_amulet_motive_0": preload("res://assets/audio/va/druid_amulet_motive_0.wav"),
	"druid_amulet_motive_1": preload("res://assets/audio/va/druid_amulet_motive_1.wav"),
	"druid_amulet_motive_insight_0": preload("res://assets/audio/va/druid_amulet_motive_insight_0.wav"),
	"druid_greeting": preload("res://assets/audio/va/druid_greeting.wav"),
	"druid_intro_0": preload("res://assets/audio/va/druid_intro_0.wav"),
	"druid_where": preload("res://assets/audio/va/druid_where.wav"),
	"druid_where_amulet_0": preload("res://assets/audio/va/druid_where_amulet_0.wav"),
	"druid_where_amulet_1": preload("res://assets/audio/va/druid_where_amulet_1.wav"),
	"druid_where_night_0": preload("res://assets/audio/va/druid_where_night_0.wav"),
	"druid_where_night_1": preload("res://assets/audio/va/druid_where_night_1.wav"),
	"druid_where_night_insight_0": preload("res://assets/audio/va/druid_where_night_insight_0.wav"),
	"druid_where_night_insight_1": preload("res://assets/audio/va/druid_where_night_insight_1.wav"),
	"druid_where_night_persuasion_0": preload("res://assets/audio/va/druid_where_night_persuasion_0.wav"),
	"druid_where_night_persuasion_1": preload("res://assets/audio/va/druid_where_night_persuasion_1.wav"),
	"druid_where_party_0": preload("res://assets/audio/va/druid_where_party_0.wav"),
	"druid_where_party_1": preload("res://assets/audio/va/druid_where_party_1.wav"),
	"druid_where_party_insight_0": preload("res://assets/audio/va/druid_where_party_insight_0.wav"),
	"druid_where_party_persuasion_0": preload("res://assets/audio/va/druid_where_party_persuasion_0.wav"),
	"druid_you": preload("res://assets/audio/va/druid_you.wav"),
	"druid_you_group_0": preload("res://assets/audio/va/druid_you_group_0.wav"),
	"druid_you_group_1": preload("res://assets/audio/va/druid_you_group_1.wav"),
	"druid_you_group_2": preload("res://assets/audio/va/druid_you_group_2.wav"),
	"druid_you_skills_0": preload("res://assets/audio/va/druid_you_skills_0.wav"),
	"druid_you_skills_1": preload("res://assets/audio/va/druid_you_skills_1.wav"),
	"druid_you_skills_2": preload("res://assets/audio/va/druid_you_skills_2.wav"),
	"druid_persuasion_fail_0": preload("res://assets/audio/va/druid_persuasion_fail_0.wav"),
	"druid_persuasion_fail_1": preload("res://assets/audio/va/druid_persuasion_fail_1.wav"),
	"druid_amulet_know_option_1": preload("res://assets/audio/va/accessibility/druid_amulet_know_option_1.wav"),
	"druid_amulet_know_option_2": preload("res://assets/audio/va/accessibility/druid_amulet_know_option_2.wav"),
	"druid_amulet_motive_option_1": preload("res://assets/audio/va/accessibility/druid_amulet_motive_option_1.wav"),
	"druid_death": preload("res://assets/audio/va/accessibility/druid_death.wav"),
	"druid_where_night_option_1": preload("res://assets/audio/va/accessibility/druid_where_night_option_1.wav"),
	"druid_where_night_option_2": preload("res://assets/audio/va/accessibility/druid_where_night_option_2.wav"),
	"druid_where_night_option_3": preload("res://assets/audio/va/accessibility/druid_where_night_option_3.wav"),
	"druid_where_party_option_1": preload("res://assets/audio/va/accessibility/druid_where_party_option_1.wav"),
	"druid_where_party_option_2": preload("res://assets/audio/va/accessibility/druid_where_party_option_2.wav"),
	"druid_where_party_option_3": preload("res://assets/audio/va/accessibility/druid_where_party_option_3.wav"),
	"1": preload("res://assets/audio/va/accessibility/1.wav"),
	"2": preload("res://assets/audio/va/accessibility/2.wav"),
	"3": preload("res://assets/audio/va/accessibility/3.wav"),
	"druid_main": preload("res://assets/audio/va/druid_main.wav"),
}

func play_sound(sound_name):
	print(self.name, " playing sound: ", sound_name)
	if !self.sounds.has(sound_name):
		print(self.name, " skipping sound: ", sound_name)
		return
	self.stream = self.sounds[sound_name]
	self.play()

func play_sounds(sounds):
	for sound in sounds:
		self.play_sound(sound)
		yield(self, "finished")

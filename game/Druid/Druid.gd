extends Dialogue

onready var ch = Rakugo.define_character("Druid" if !Rakugo.store.get("druid_dead") else "Druid (Ghost)", "druid", Color("#117733"))
onready var druidAudio = get_node("../DruidAudio")

"""
About you
	What are you good at?
	Why are you with this group?
About your whereabouts
	Where were you during the party?
	Where were you last night?
	Where were you when the amulet was discovered?
About the amulet
	What do you know about the amulet?
	What would you do if you had the amulet?
	What was it like when the amulet was discovered?
"""

func first_dialogue():
	start_event("first_dialogue")
	Tavern.change_image("Tavern")
	druidAudio.play_sound("druid_greeting")
	say("druid", "Oh, hi! Sorry, I'm a little nervous around people.")
	
	step()
	
	druidAudio.play_sound("druid_main")
	say("druid", "What's your question?")
	var start_topic = menu([
		["About you", "you", {}],
		["About your whereabouts", "where", {}],
		["About the amulet", "amulet", {}],
		["No more questions.", "done", {}]
	])
	step()
	
	if cond(start_topic == "you"):
		jump("", "", "about_you")
	elif cond(start_topic == "where"):
		jump("", "", "about_where")
	elif cond(start_topic == "amulet"):
		jump("", "", "about_amulet")
	else:
		Music.play_tavern()
		jump("CharacterChoose", "", "select_character")
	
	end_event()

func about_you():
	start_event("about_you")
	Tavern.change_image("Question")
	druidAudio.play_sound("druid_you")
	say("druid", "What would you like to know about me?")
	var choice = menu([
			["What are you good at?", "skills", {}],
			["Why are you with this group?", "group", {}],
			["Never mind.", "back", {}]
		])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "skills"):
		jump("", "", "you_skills")
	elif cond(choice == "group"):
		jump("", "", "you_group")
	end_event()

func you_skills():
	start_event("you_skills")
	
	step()
	druidAudio.play_sound("druid_you_skills_0")
	say("druid", "Um, well, I’m good with nature. Nature is cool! Sometimes better than people. People can be scary.")
	step()
	druidAudio.play_sound("druid_you_skills_1")
	say("druid", "Oh, I’m particularly good with, uh, animals! I can turn into animals if I want.")
	step()
	druidAudio.play_sound("druid_you_skills_2")
	say("druid", "Ah, that’s probably relevant information I should have included earlier. Sorry, I didn’t mean to obstruct the investigation. Please don’t hurt me!")
	step()
	
	jump("", "", "about_you")
	end_event()

func you_group():
	start_event("you_group")
	
	step()
	druidAudio.play_sound("druid_you_group_0")
	say("druid", "Sometimes I’m not even sure myself because, um, I was just minding my business one day right? Just going about the forest as you do, or maybe you don’t, sorry I don’t mean to judge.")
	step()
	druidAudio.play_sound("druid_you_group_1")
	say("druid", "Uh, anyway, this group of adventurers were walking through and then they were like “ok everyone, time to go” and they looked at me and they were like “aren’t you coming?”")
	step()
	druidAudio.play_sound("druid_you_group_2")
	say("druid", "And I didn’t wanna say no because, I mean, they looked really powerful and I didn’t wanna get on their bad side and also correcting them would have been really awkward and I can’t do awkwardness! So, I just sort of went along with them and I’ve been part of the group ever since. Yeah. It’s uh… it’s been a time.")
	step()
	
	jump("", "", "about_you")
	end_event()

func about_where():
	start_event("about_where")
	Tavern.change_image("Tavern")
	druidAudio.play_sound("druid_where")
	say("druid", "What time are you asking about?")
	var choice = menu([
		["Where were you when the amulet was discovered?", "amulet", {}],
		["Where were you during the party?", "party", {}],
		["Where were you after the party?", "night", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "party"):
		jump("", "", "where_party")
	elif cond(choice == "night"):
		jump("", "", "where_night")
	elif cond(choice == "amulet"):
		jump("", "", "where_amulet")
	
	end_event()

func where_party():
	start_event("where_party")
	Tavern.change_image("Party")
	step()
	druidAudio.play_sound("druid_where_party_0")
	say("druid", "The bard wanted me to join in some kind of drinking game and do… I think it was called karaoke? Some kind of singing thing. But that was such an uncomfortable idea I actually managed to say no!")
	step()
	druidAudio.play_sound("druid_where_party_1")
	say("druid", "I stopped by the wizard’s table briefly to see how her studies were coming, but she was so focused on her work I’m not sure she noticed me at all. I left pretty early because these parties are just really not for me.")
	step()
	
	jump("", "", "where_party_menu")
	
	end_event()

func where_party_menu():
	start_event("where_party_menu")
	
	step()
	if Settings.get("accessibility"):
		druidAudio.play_sounds(["1", "druid_where_party_option_1", "2", "druid_where_party_option_2", "3", "druid_where_party_option_3"])
	say(null, """1. Sounds like you were moving around quite a bit! Did you notice anything else?
2. Thank you! Your observational skills are really paying off. Did you notice anything else?
3. Are you sure you’re presenting that course of events accurately?""")
	var choice = menu([
		["1", "persuasion_fail", {}],
		["2", "persuasion", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "where_party_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("druid_dead"):
			jump("", "", "where_party_persuasion")
		else:
			Rakugo.store.set("druid_dead", true)
			jump("", "", "where_party_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_party_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_party_persuasion_fail():
	start_event("where_party_persuasion_fail")
	
	step()
	druidAudio.play_sound("druid_persuasion_fail_0")
	say("druid", "Um, can I go now? Sorry, it's just that you've been going over a lot and it's overwhelming and I want to leave now and you can't stop me! Or maybe you can...")
	step()
	ch.name = "Druid (Ghost)"
	druidAudio.play_sound("druid_death")
	say(null, "Her weak threat had an effect, but not the desired one. Vines sprouted out of nowhere and wrapped tightly around her. She was choked with alarming speed, and then she was breathing no more. The vines withered away to reveal a ghost.")
	step()
	druidAudio.play_sound("druid_persuasion_fail_1")
	say("druid", "Oh, it looks like I'm still here? I think I was answering a question?")
	step()
	jump("", "", "where_party_persuasion")
	
	end_event()

func where_party_persuasion():
	start_event("where_party_persuasion")
	
	step()
	druidAudio.play_sound("druid_where_party_persuasion_0")
	say("druid", "The cleric and the paladin were talking for quite a while. No idea what they said, but some kind of conversation was happening. That’s all I saw, though, sorry.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_party_insight():
	start_event("where_party_insight")
	
	step()
	druidAudio.play_sound("druid_where_party_insight_0")
	say("druid", "I told you exactly what I did, no more, no less. I promise!")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_night():
	start_event("where_night")
	Tavern.change_image("Bedroom")
	step()
	druidAudio.play_sound("druid_where_night_0")
	say("druid", "Well I wanted to go back to the forest, get away from all this for a bit, but I also wanted to make sure I was there for the loot division tomorrow. So I compromised and took a moonlit walk, not straying too far from the tavern.")
	step()
	druidAudio.play_sound("druid_where_night_1")
	say("druid", "I came back as the party was finishing and I went to bed. I slept in the third room on the left.")
	step()
	
	jump("", "", "where_night_menu")

	end_event()

func where_night_menu():
	start_event("where_night_menu")
	
	step()
	if Settings.get("accessibility"):
		druidAudio.play_sounds(["1", "druid_where_night_option_1", "2", "druid_where_night_option_2", "3", "druid_where_night_option_3"])
	say(null, """1. A very smart strategy. Did you notice anyone else?
2. Good to get away from people, right? Did you notice anyone else?
3. Are you sure you didn’t do anything else?""")
	var choice = menu([
		["1", "persuasion_fail", {}],
		["2", "persuasion", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "where_night_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("druid_dead"):
			jump("", "", "where_night_persuasion")
		else:
			Rakugo.store.set("druid_dead", true)
			jump("", "", "where_night_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_night_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_night_persuasion_fail():
	start_event("where_night_persuasion_fail")
	
	step()
	druidAudio.play_sound("druid_persuasion_fail_0")
	say("druid", "Um, can I go now? Sorry, it's just that you've been going over a lot and it's overwhelming and I want to leave now and you can't stop me! Or maybe you can...")
	step()
	ch.name = "Druid (Ghost)"
	druidAudio.play_sound("druid_death")
	say(null, "Her weak threat had an effect, but not the desired one. Vines sprouted out of nowhere and wrapped tightly around her. She was choked with alarming speed, and then she was breathing no more. The vines withered away to reveal a ghost.")
	step()
	druidAudio.play_sound("druid_persuasion_fail_1")
	say("druid", "Oh, it looks like I'm still here? I think I was answering a question?")
	step()
	jump("", "", "where_night_persuasion")
	
	end_event()

func where_night_persuasion():
	start_event("where_night_persuasion")
	
	step()
	druidAudio.play_sound("druid_where_night_persuasion_0")
	say("druid", "I passed by the rogue on the way out. It looked like she was fighting some rats? Kind of a strange way to pass the night if you ask me, but I try not to judge.")
	step()
	druidAudio.play_sound("druid_where_night_persuasion_1")
	say("druid", "Oh, and when I was outside I heard a ways away from me something that sounded like fire. I can’t be sure who or what that was, but I do know the sorcerer is often starting fires.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_night_insight():
	start_event("where_night_insight")
	
	step()
	druidAudio.play_sound("druid_where_night_insight_0")
	say("druid", "Well, I did turn into a rat. I don’t like to draw too much attention, so I figured that was relatively inconspicuous. As a rat, I went over to the rooms on the right.")
	step()
	druidAudio.play_sound("druid_where_night_insight_1")
	say("druid", "I was thinking after all that business with the amulet I was worried there might be something fishy going on. But I didn’t hear anything.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_amulet():
	start_event("where_amulet")
	Tavern.change_image("Treasure")
	step()
	druidAudio.play_sound("druid_where_amulet_0")
	say("druid", "I think at that point I was still a bear, cause we had fought some enemies not that long ago, and I wasn’t certain that there weren’t more around somewhere.")
	step()
	druidAudio.play_sound("druid_where_amulet_1")
	say("druid", "So I was hanging near the back of the group in case anything came after us. I was a little nervous about that but I know bears are strong and healthy so I was thinking “this is probably fine.” And I suppose it was.")
	step()
	
	jump("", "", "about_where")
	end_event()

func about_amulet():
	start_event("about_amulet")
	Tavern.change_image("Amulet")
	druidAudio.play_sound("druid_amulet")
	say("druid", "What would you like to know about the amulet?")
	var choice = menu([
		["What do you know about the amulet?", "know", {}],
		["What would you do if you had the amulet?", "motive", {}],
		["What was it like when the amulet was discovered?", "discovered", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "know"):
		jump("", "", "amulet_know")
	elif cond(choice == "motive"):
		jump("", "", "amulet_motive")
	elif cond(choice == "discovered"):
		jump("", "", "amulet_discovered")
	end_event()

func amulet_know():
	start_event("amulet_know")
	
	step()
	druidAudio.play_sound("druid_amulet_know_0")
	say("druid", "I mean, it’s some kind of a protection thing, right? I don’t really know how the protection works, but I assume some sort of warding off danger.")
	step()
	druidAudio.play_sound("druid_amulet_know_1")
	say("druid", "It didn’t look like the sort of thing to protect people by transforming them, but I really don’t know. I’m really sorry I couldn’t be more helpful.")
	step()
	
	jump("", "", "amulet_know_menu")

	end_event()

func amulet_know_menu():
	start_event("amulet_know_menu")
	
	step()
	if Settings.get("accessibility"):
		druidAudio.play_sounds(["1", "druid_amulet_know_option_1", "2", "druid_amulet_know_option_2"])
	say(null, """1. That’s a promising theory. Tell me more about this transforming people.
2. Are you sure that’s all?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "amulet_know_persuasion")
	elif cond(choice == "insight"):
		jump("", "", "amulet_know_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_know_persuasion():
	start_event("amulet_know_persuasion")
	
	step()
	druidAudio.play_sound("druid_amulet_know_persuasion_0")
	say("druid", "Oh, I don’t know, it seems like the sort of wish-master kind of deal that a powerful magical item might have.")
	step()
	druidAudio.play_sound("druid_amulet_know_persuasion_1")
	say("druid", "Like, if I turn you into stone you’d be a lot safer, but you’d also be stone. I don’t think that’s what’s happening here, but I could be totally wrong on that.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_insight():
	start_event("amulet_know_insight")
	
	step()
	druidAudio.play_sound("druid_amulet_know_insight_0")
	say("druid", "I’m sure that’s not all, but I’m also sure I’ve told you all I know.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_motive():
	start_event("amulet_motive")
	
	step()
	druidAudio.play_sound("druid_amulet_motive_0")
	say("druid", "Oh, if I had the amulet? I would use it for my sister. I wish she could have come along with us: I would much rather have her where I can watch out for her. But I’m afraid she’s been ill for quite some time now.")
	step()
	druidAudio.play_sound("druid_amulet_motive_1")
	say("druid", "In my searches I’m always keeping an eye out for some kind of cure, but I haven’t found anything yet. But I think protection could at least buy her a little more time until I find something to help her.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_motive_menu():
	start_event("amulet_motive_menu")
	
	step()
	if Settings.get("accessibility"):
		druidAudio.play_sounds(["1", "druid_amulet_motive_option_1"])
	say(null, """1. Is that really what you would use the amulet for?""")
	var choice = menu([
		["1", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "insight"):
		jump("", "", "amulet_motive_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_motive_insight():
	start_event("amulet_motive_insight")
	
	step()
	druidAudio.play_sound("druid_amulet_motive_insight_0")
	say("druid", "Yes, definitely. She’s the most important person to me so of course I’d want to protect her.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_discovered():
	start_event("amulet_discovered")
	
	step()
	druidAudio.play_sound("druid_amulet_discovered_0")
	say("druid", "Ah, sorry, I was rather distracted. See, I was a bear at the time, and I couldn’t stop thinking about how the water dripping from the cave sounded a little bit like a stream and oh maybe there would be fish in the stream and wouldn’t fish be just the thing I need right now?")
	step()
	druidAudio.play_sound("druid_amulet_discovered_1")
	say("druid", "I really need to remember to carry rations for these long trips, but also part of that is I do get a little bit caught up in whatever animal I’m acting as.")
	step()
	druidAudio.play_sound("druid_amulet_discovered_2")
	say("druid", "Oh, right the amulet! Um, someone picked it up, I think it was the cleric, and then put it away.")
	step()
	
	jump("", "", "about_amulet")
	end_event()

extends Node2D

var tavern_pos = 0
var detective_pos = 0

func play_tavern():
	detective_pos = $DetectiveMusic.get_playback_position()
	$DetectiveMusic.stop()
	$TavernMusic.play(tavern_pos)

func play_detective():
	tavern_pos = $TavernMusic.get_playback_position()
	$TavernMusic.stop()
	$DetectiveMusic.play(detective_pos)

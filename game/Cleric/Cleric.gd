extends Dialogue

onready var ch = Rakugo.define_character("Cleric" if !Rakugo.store.get("cleric_dead") else "Cleric (Ghost)", "cleric", Color("#AA4499"))
onready var clericAudio = get_node("../ClericAudio")

"""
About you
	What are you good at?
	Why are you with this group?
About your whereabouts
	Where were you during the party?
	Where were you last night?
	Where were you when the amulet was discovered?
About the amulet
	What do you know about the amulet?
	What would you do if you had the amulet?
	What was it like when the amulet was discovered?
"""

func first_dialogue():
	start_event("first_dialogue")
	Tavern.change_image("Tavern")
	
	clericAudio.play_sound("cleric_greeting")
	say("cleric", "Hello. Thanks for helping us get to the bottom of this.")
	step()
	
	clericAudio.play_sound("cleric_main")
	say("cleric", "How can I help you?")
	var start_topic = menu([
		["About you", "you", {}],
		["About your whereabouts", "where", {}],
		["About the amulet", "amulet", {}],
		["No more questions.", "done", {}]
	])
	step()
	
	if cond(start_topic == "you"):
		jump("", "", "about_you")
	elif cond(start_topic == "where"):
		jump("", "", "about_where")
	elif cond(start_topic == "amulet"):
		jump("", "", "about_amulet")
	else:
		Music.play_tavern()
		jump("CharacterChoose", "", "select_character")
	
	end_event()

func about_you():
	start_event("about_you")
	Tavern.change_image("Question")
	clericAudio.play_sound("cleric_you")
	say("cleric", "Alright, what do you want to know about me?")
	var choice = menu([
			["What are you good at?", "skills", {}],
			["Why are you with this group?", "group", {}],
			["Never mind.", "back", {}]
		])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "skills"):
		jump("", "", "you_skills")
	elif cond(choice == "group"):
		jump("", "", "you_group")
	
	end_event()

func you_skills():
	start_event("you_skills")
	
	step()
	clericAudio.play_sound("cleric_you_skills_0")
	say("cleric", "I mean, I can do a variety of things, but mostly I end up providing healing and support. I can take quite a few hits as well, and every now and then we need more people in that role, so I’m happy to step up.")
	step()
	clericAudio.play_sound("cleric_you_skills_1")
	say("cleric", "I also have a wide variety of religious knowledge and some history and culture knowledge, though I wouldn’t say I know quite as much as our lovely wizard.")
	step()
	
	jump("", "", "about_you")
	end_event()

func you_group():
	start_event("you_group")
	
	step()
	clericAudio.play_sound("cleric_you_group_0")
	say("cleric", "Well it’s a little funny to think about now, but I actually serve the god of protection. After I finished my cleric training, he told me to find a group that could use some protection, and I found this sorry lot.")
	step()
	clericAudio.play_sound("cleric_you_group_1")
	say("cleric", "Well, not all of them. The paladin came later, and I’m very grateful for her influence.")
	step()
	
	jump("", "", "about_you")
	end_event()

func about_where():
	start_event("about_where")
	Tavern.change_image("Tavern")
	clericAudio.play_sound("cleric_where")
	say("cleric", "Good idea! What time are you asking about?")
	var choice = menu([
		["Where were you when the amulet was discovered?", "amulet", {}],
		["Where were you during the party?", "party", {}],
		["Where were you after the party?", "night", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "party"):
		jump("", "", "where_party")
	elif cond(choice == "night"):
		jump("", "", "where_night")
	elif cond(choice == "amulet"):
		jump("", "", "where_amulet")
	
	end_event()

func where_party():
	start_event("where_party")
	Tavern.change_image("Party")
	step()
	clericAudio.play_sound("cleric_where_party_0")
	say("cleric", "I was hanging around near the middle, trying to keep an eye on everyone. I know we all like to have fun, but I always get a little nervous someone will let their guard down or get a little too rowdy, and we really don’t want that kind of trouble. Or at least, I don’t.")
	step()
	clericAudio.play_sound("cleric_where_party_1")
	say("cleric", "Of course, that doesn’t mean I want to stand around doing nothing. I had a whisky to help lighten up a little, and it was a good whisky too. I made sure to leave a nice tip.")
	step()
	clericAudio.play_sound("cleric_where_party_2")
	say("cleric", "I talked with the paladin for a while, and I was glad I’d had that whisky. She’s a good sort, but she can be so stuffy sometimes.")
	step()
	clericAudio.play_sound("cleric_where_party_3")
	say("cleric", "Oh, and I had a brief break for singing with the bard. That was fun. All in all, a good evening.")
	step()
	
	jump("", "", "where_party_menu")
	
	end_event()

func where_party_menu():
	start_event("where_party_menu")
	
	step()
	if Settings.get("accessibility"):
		clericAudio.play_sounds(["1", "cleric_where_party_option_1", "2", "cleric_where_party_option_2", "3", "cleric_where_party_option_3"])
	say(null, """1. I don’t mean to insult you, but do you think you may have been distracted enough that someone could have stolen from you during the party?
2. Sounds like there was a lot going on. Do you think someone may have found an opportunity to steal from you during the party?
3. Are you sure you’re not leaving anything out?""")
	var choice = menu([
		["1", "persuasion_fail", {}],
		["2", "persuasion", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_party_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("cleric_dead"):
			jump("", "", "where_party_persuasion")
		else:
			Rakugo.store.set("cleric_dead", true)
			jump("", "", "where_party_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_party_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_party_persuasion_fail():
	start_event("where_party_persuasion_fail")
	step()
	clericAudio.play_sound("cleric_persuasion_fail_0")
	say("cleric", "I'm beginning to think we should have chosen our investigator more carefully. You asked strange questions, and it doesn't seem like you're any closer to solving this. I wonder if we shouldn't just get rid of you and find someone else.")
	step()
	ch.name = "Cleric (Ghost)"
	clericAudio.play_sound("cleric_death")
	say(null, "I was hoping she meant that in a nonlethal way. I didn't get a chance to find out, though. A flash of light covered her, and then she disintegrated. After a while, the particles slowly reformed themselves into a ghost.")
	step()
	clericAudio.play_sound("cleric_persuasion_fail_1")
	say("cleric", "Anyway, as I was saying...")
	step()
	jump("", "", "where_party_persuasion")
	
	end_event()

func where_party_persuasion():
	start_event("where_party_persuasion")
	
	step()
	clericAudio.play_sound("cleric_where_party_persuasion_0")
	say("cleric", "Unfortunately, I think you may be onto something. I wouldn’t rule it out as a possibility. I could have been pickpocketed by someone sneaky enough: the rogue and the druid are both capable of that.")
	step()
	clericAudio.play_sound("cleric_where_party_persuasion_1")
	say("cleric", "Or maybe I could have been magically charmed by those more charismatic sorts: the bard, the paladin, or the sorcerer.")
	step()
	clericAudio.play_sound("cleric_where_party_persuasion_2")
	say("cleric", "I’m forgetting someone: the wizard! I mean, I suppose she could have used some sort of magic to steal from me: maybe she could have transformed the amulet to make it easier to steal, or moved it magically?")
	step()
	clericAudio.play_sound("cleric_where_party_persuasion_3")
	say("cleric", "Oh, it’s so hard to tell with these things. My god, magic is complicated! Uh, sorry God.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_party_insight():
	start_event("where_party_insight")
	
	step()
	clericAudio.play_sound("cleric_where_party_insight_0")
	say("cleric", "Well, there was one more thing. The paladin and I talked a little bit about how the amulet could be dangerous and how there were many people in the group we didn’t trust with it.")
	step()
	clericAudio.play_sound("cleric_where_party_insight_1")
	say("cleric", "Looks like we were right to be concerned, since someone stole it!")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_night():
	start_event("where_night")
	Tavern.change_image("Bedroom")
	step()
	clericAudio.play_sound("cleric_where_night_0")
	say("cleric", "I was rather tired from the whole thing, so I went straight to bed. Sorry if that’s a bit boring. Oh, right, you’ll be wondering where that is. I was in the first room on the right.")
	step()
	
	jump("", "", "where_night_menu")
	
	end_event()

func where_night_menu():
	start_event("where_night_menu")
	
	step()
	if Settings.get("accessibility"):
		clericAudio.play_sounds(["1", "cleric_where_night_option_1", "2", "cleric_where_night_option_2", "3", "cleric_where_night_option_3"])
	say(null, """1. How good is your hearing?
2. If I might ask a personal question, how sound of a sleeper are you?
3. Are you sure you’ve given me all the details about last night?""")
	var choice = menu([
		["1", "persuasion_fail", {}],
		["2", "persuasion", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "where_night_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("cleric_dead"):
			jump("", "", "where_night_persuasion")
		else:
			Rakugo.store.set("cleric_dead", true)
			jump("", "", "where_night_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_night_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_night_persuasion_fail():
	start_event("where_night_persuasion_fail")
	step()
	clericAudio.play_sound("cleric_persuasion_fail_0")
	say("cleric", "I'm beginning to think we should have chosen our investigator more carefully. You asked strange questions, and it doesn't seem like you're any closer to solving this. I wonder if we shouldn't just get rid of you and find someone else.")
	step()
	ch.name = "Cleric (Ghost)"
	clericAudio.play_sound("cleric_death")
	say(null, "I was hoping she meant that in a nonlethal way. I didn't get a chance to find out, though. A flash of light covered her, and then she disintegrated. After a while, the particles slowly reformed themselves into a ghost.")
	step()
	clericAudio.play_sound("cleric_persuasion_fail_1")
	say("cleric", "Anyway, as I was saying...")
	step()
	jump("", "", "where_night_persuasion")
	
	end_event()

func where_night_persuasion():
	start_event("where_night_persuasion")
	
	step()
	clericAudio.play_sound("cleric_where_night_persuasion_0")
	say("cleric", "Ah, I think I know where you’re going with this. Yes, I’m a rather sound sleeper. If anything happened last night I wouldn’t have noticed, unless perhaps it was something extreme like a loud explosion or the roof caving in.")
	step()
	clericAudio.play_sound("cleric_where_night_persuasion_1")
	say("cleric", "And I didn’t notice anything last night. Shame on me, on us all. We really need to set up a regular schedule of watches.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_night_insight():
	start_event("where_night_insight")
	
	step()
	clericAudio.play_sound("cleric_where_night_insight_0")
	say("cleric", "Well, I do take my jewelry off to sleep. It’s just too uncomfortable otherwise. I think I probably took off the amulet, too. But for the life of me, I can’t remember whether or not I had the amulet at that point.")
	step()
	clericAudio.play_sound("cleric_where_night_insight_1")
	say("cleric", "I’m in hot water now, aren’t I? Such an incompetent leader, surely they won’t keep me in charge after this. I hope the rule goes to the paladin, I feel like she knows what she’s doing.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_amulet():
	start_event("where_amulet")
	Tavern.change_image("Treasure")
	step()
	clericAudio.play_sound("cleric_where_amulet_0")
	say("cleric", "I was the one discovering the amulet. I don’t know why you’d bother asking me that. Sorry, I’ll try not to question your methods. Anyway, I opened the chest and took the amulet. Pretty straightforward.")
	step()
	
	jump("", "", "about_where")
	end_event()

func about_amulet():
	start_event("about_amulet")
	
	say("cleric", "The item that got us into this mess. What's your question?")
	var choice = menu([
		["What do you know about the amulet?", "know", {}],
		["What would you do if you had the amulet?", "motive", {}],
		["What was it like when the amulet was discovered?", "discovered", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "know"):
		jump("", "", "amulet_know")
	elif cond(choice == "motive"):
		jump("", "", "amulet_motive")
	elif cond(choice == "discovered"):
		jump("", "", "amulet_discovered")
	end_event()

func amulet_know():
	start_event("amulet_know")
	
	step()
	clericAudio.play_sound("cleric_amulet_know_0")
	say("cleric", "Only what I’ve learned from the wizard. It’s a magical amulet that provides protection of some sort but also has some kind of dangerous curse.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_menu():
	start_event("amulet_know_menu")
	
	step()
	if Settings.get("accessibility"):
		clericAudio.play_sounds(["1", "cleric_amulet_know_option_1", "2", "cleric_amulet_know_option_2"])
	say(null, """1. Can you tell me who else I should talk to about the amulet?
2. Are you sure you don’t know anything else about the amulet?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "amulet_know_persuasion")
	elif cond(choice == "insight"):
		jump("", "", "amulet_know_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_know_persuasion():
	start_event("amulet_know_persuasion")
	
	step()
	clericAudio.play_sound("cleric_amulet_know_persuasion_0")
	say("cleric", "Definitely talk to the wizard, if you haven’t already.")
	step()
	clericAudio.play_sound("cleric_amulet_know_persuasion_1")
	say("cleric", "As for everyone else, well they all have their theories, but I’m not sure how important they are to the theft, which is the main thing you’re investigating. Maybe talk to the bard if you like?")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_insight():
	start_event("amulet_know_insight")
	
	step()
	clericAudio.play_sound("cleric_amulet_know_insight_0")
	say("cleric", "Actually, I do know a little bit more, I was just wary of sharing that information. I’m sorry, you’re only trying to help.")
	step()
	clericAudio.play_sound("cleric_amulet_know_insight_1")
	say("cleric", "Here’s what else you should know: the curse detects hostility in a group and neutralizes one of the people who is hostile. It’s a right nasty curse.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_motive():
	start_event("amulet_motive")
	
	step()
	clericAudio.play_sound("cleric_amulet_motive_0")
	say("cleric", "I think it requires a bit more studying first to figure out what it can reasonably be used for. Even if it weren’t cursed, items like these tend to have unintended consequences so it’s only responsible to prepare for that.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_motive_menu():
	start_event("amulet_motive_menu")
	
	step()
	if Settings.get("accessibility"):
		clericAudio.play_sounds(["1", "cleric_amulet_motive_option_1"])
	say(null, """1. You wouldn’t use the amulet?""")
	var choice = menu([
		["1", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "insight"):
		jump("", "", "amulet_motive_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_motive_insight():
	start_event("amulet_motive_insight")
	
	step()
	clericAudio.play_sound("cleric_amulet_motive_insight_0")
	say("cleric", "Not right away, no. I’m very wary of these things. They need to be used responsibly.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_discovered():
	start_event("amulet_discovered")
	
	step()
	clericAudio.play_sound("cleric_amulet_discovered_0")
	say("cleric", "I guess I was a little too obvious in my excitement about this thing, because even though I put it away before anyone could get a good look, everyone was in uproar. All shouting about who should have this thing first.")
	step()
	clericAudio.play_sound("cleric_amulet_discovered_1")
	say("cleric", "It’s like they all forgot those rules we agreed on. This group can be so frustrating sometimes.")
	
	jump("", "", "about_amulet")
	end_event()

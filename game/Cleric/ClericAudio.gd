extends AudioStreamPlayer

const sounds = {
	"cleric_amulet_discovered_0": preload("res://assets/audio/va/cleric_amulet_discovered_0.wav"),
	"cleric_amulet_know_0": preload("res://assets/audio/va/cleric_amulet_know_0.wav"),
	"cleric_amulet_know_insight_0": preload("res://assets/audio/va/cleric_amulet_know_insight_0.wav"),
	"cleric_amulet_know_persuasion_0": preload("res://assets/audio/va/cleric_amulet_know_persuasion_0.wav"),
	"cleric_amulet_know_persuasion_1": preload("res://assets/audio/va/cleric_amulet_know_persuasion_1.wav"),
	"cleric_amulet_motive_0": preload("res://assets/audio/va/cleric_amulet_motive_0.wav"),
	"cleric_amulet_motive_insight_0": preload("res://assets/audio/va/cleric_amulet_motive_insight_0.wav"),
	"cleric_greeting": preload("res://assets/audio/va/cleric_greeting.wav"),
	"cleric_intro_0": preload("res://assets/audio/va/cleric_intro_0.wav"),
	"cleric_intro_1": preload("res://assets/audio/va/cleric_intro_1.wav"),
	"cleric_intro_2": preload("res://assets/audio/va/cleric_intro_2.wav"),
	"cleric_intro_3": preload("res://assets/audio/va/cleric_intro_3.wav"),
	"cleric_intro_4": preload("res://assets/audio/va/cleric_intro_4.wav"),
	"cleric_main": preload("res://assets/audio/va/cleric_main.wav"),
	"cleric_where": preload("res://assets/audio/va/cleric_where.wav"),
	"cleric_where_amulet_0": preload("res://assets/audio/va/cleric_where_amulet_0.wav"),
	"cleric_where_night_0": preload("res://assets/audio/va/cleric_where_night_0.wav"),
	"cleric_where_night_insight_0": preload("res://assets/audio/va/cleric_where_night_insight_0.wav"),
	"cleric_where_night_insight_1": preload("res://assets/audio/va/cleric_where_night_insight_1.wav"),
	"cleric_where_night_persuasion_0": preload("res://assets/audio/va/cleric_where_night_persuasion_0.wav"),
	"cleric_where_night_persuasion_1": preload("res://assets/audio/va/cleric_where_night_persuasion_1.wav"),
	"cleric_where_party_0": preload("res://assets/audio/va/cleric_where_party_0.wav"),
	"cleric_where_party_1": preload("res://assets/audio/va/cleric_where_party_1.wav"),
	"cleric_where_party_2": preload("res://assets/audio/va/cleric_where_party_2.wav"),
	"cleric_where_party_3": preload("res://assets/audio/va/cleric_where_party_3.wav"),
	"cleric_where_party_insight_0": preload("res://assets/audio/va/cleric_where_party_insight_0.wav"),
	"cleric_where_party_insight_1": preload("res://assets/audio/va/cleric_where_party_insight_1.wav"),
	"cleric_where_party_persuasion_0": preload("res://assets/audio/va/cleric_where_party_persuasion_0.wav"),
	"cleric_where_party_persuasion_1": preload("res://assets/audio/va/cleric_where_party_persuasion_1.wav"),
	"cleric_where_party_persuasion_2": preload("res://assets/audio/va/cleric_where_party_persuasion_2.wav"),
	"cleric_where_party_persuasion_3": preload("res://assets/audio/va/cleric_where_party_persuasion_3.wav"),
	"cleric_you": preload("res://assets/audio/va/cleric_you.wav"),
	"cleric_you_group_0": preload("res://assets/audio/va/cleric_you_group_0.wav"),
	"cleric_you_group_1": preload("res://assets/audio/va/cleric_you_group_1.wav"),
	"cleric_you_skills_0": preload("res://assets/audio/va/cleric_you_skills_0.wav"),
	"cleric_you_skills_1": preload("res://assets/audio/va/cleric_you_skills_1.wav"),
	"cleric_persuasion_fail_0": preload("res://assets/audio/va/cleric_persuasion_fail_0.wav"),
	"cleric_persuasion_fail_1": preload("res://assets/audio/va/cleric_persuasion_fail_1.wav"),
	"cleric_amulet_know_option_1": preload("res://assets/audio/va/accessibility/cleric_amulet_know_option_1.wav"),
	"cleric_amulet_know_option_2": preload("res://assets/audio/va/accessibility/cleric_amulet_know_option_2.wav"),
	"cleric_amulet_motive_option_1": preload("res://assets/audio/va/accessibility/cleric_amulet_motive_option_1.wav"),
	"cleric_death": preload("res://assets/audio/va/accessibility/cleric_death.wav"),
	"cleric_where_night_option_1": preload("res://assets/audio/va/accessibility/cleric_where_night_option_1.wav"),
	"cleric_where_night_option_2": preload("res://assets/audio/va/accessibility/cleric_where_night_option_2.wav"),
	"cleric_where_night_option_3": preload("res://assets/audio/va/accessibility/cleric_where_night_option_3.wav"),
	"cleric_where_party_option_1": preload("res://assets/audio/va/accessibility/cleric_where_party_option_1.wav"),
	"cleric_where_party_option_2": preload("res://assets/audio/va/accessibility/cleric_where_party_option_2.wav"),
	"cleric_where_party_option_3": preload("res://assets/audio/va/accessibility/cleric_where_party_option_3.wav"),
	"1": preload("res://assets/audio/va/accessibility/1.wav"),
	"2": preload("res://assets/audio/va/accessibility/2.wav"),
	"3": preload("res://assets/audio/va/accessibility/3.wav"),
	"cleric_amulet": preload("res://assets/audio/va/cleric_amulet.wav"),
	"cleric_amulet_discovered_1": preload("res://assets/audio/va/cleric_amulet_discovered_1.wav"),
	"cleric_amulet_know_insight_1": preload("res://assets/audio/va/cleric_amulet_know_insight_1.wav"),
}

func play_sound(sound_name):
	print(self.name, " playing sound: ", sound_name)
	if !self.sounds.has(sound_name):
		print(self.name, " skipping sound: ", sound_name)
		return
	self.stream = self.sounds[sound_name]
	self.play()

func play_sounds(sounds):
	for sound in sounds:
		self.play_sound(sound)
		yield(self, "finished")

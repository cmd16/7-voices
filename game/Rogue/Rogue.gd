extends Dialogue

onready var ch = Rakugo.define_character("Rogue" if !Rakugo.store.get("rogue_dead") else "Rogue (Ghost)", "rogue", Color("#332288"))
onready var rogueAudio = get_node("../RogueAudio")

"""
About you
	What are you good at?
	Why are you with this group?
About your whereabouts
	Where were you during the party?
	Where were you last night?
	Where were you when the amulet was discovered?
About the amulet
	What do you know about the amulet?
	What would you do if you had the amulet?
	What was it like when the amulet was discovered?
"""

func first_dialogue():
	start_event("first_dialogue")
	Tavern.change_image("Tavern")
	rogueAudio.play_sound("rogue_greeting")
	say("rogue", "I'd like to clarify right now, just because it's a theft doesn't mean it was me. I may be greedy, but at least I'm honest.")
	
	step()
	rogueAudio.play_sound("rogue_main")
	say("rogue", "What do you want to know?")

	var start_topic = menu([
		["About you", "you", {}],
		["About your whereabouts", "where", {}],
		["About the amulet", "amulet", {}],
		["No more questions.", "done", {}]
	])
	step()
	
	if cond(start_topic == "you"):
		jump("", "", "about_you")
	elif cond(start_topic == "where"):
		jump("", "", "about_where")
	elif cond(start_topic == "amulet"):
		jump("", "", "about_amulet")
	else:
		Music.play_tavern()
		jump("CharacterChoose", "", "select_character")
	
	end_event()

func about_you():
	start_event("about_you")
	Tavern.change_image("Question")
	rogueAudio.play_sound("rogue_you")
	say("rogue", "Go ahead and interrogate my character.")
	var choice = menu([
			["What are you good at?", "skills", {}],
			["Why are you with this group?", "group", {}],
			["Never mind.", "back", {}]
		])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "skills"):
		jump("", "", "you_skills")
	elif cond(choice == "group"):
		jump("", "", "you_group")
	end_event()

func you_skills():
	start_event("you_skills")
	
	step()
	rogueAudio.play_sound("rogue_you_skills_0")
	say("rogue", "Oh, you know, the usual rogue stuff. Scouting ahead and being all sneaky, stealing things, checking for traps, lying to get us out of trouble...")
	step()
	rogueAudio.play_sound("rogue_you_skills_1")
	say("rogue", "Not that we get into trouble that often with these do-gooders around. But hey, never hurts to be prepared, right?")
	step()
	
	jump("", "", "about_you")
	end_event()

func you_group():
	start_event("you_group")
	
	step()
	rogueAudio.play_sound("rogue_you_group_0")
	say("rogue", "Much as I like to think I’m a lone wolf, when it really comes down to it you can get a lot more done in a group. Together we’re able to explore much bigger, deadlier places that I could ever hope to survive myself.")
	step()
	rogueAudio.play_sound("rogue_you_group_1")
	say("rogue", "Does get annoying when they fail to be stealthy and keep drawing enemy attention, though. At least the treasure is good.")
	step()
	
	jump("", "", "about_you")
	end_event()

func about_where():
	start_event("about_where")
	Tavern.change_image("Tavern")
	rogueAudio.play_sound("rogue_where")
	say("rogue", "What time do you need me to account for?")
	var choice = menu([
		["Where were you when the amulet was discovered?", "amulet", {}],
		["Where were you during the party?", "party", {}],
		["Where were you after the party?", "night", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "party"):
		jump("", "", "where_party")
	elif cond(choice == "night"):
		jump("", "", "where_night")
	elif cond(choice == "amulet"):
		jump("", "", "where_amulet")
	
	end_event()

func where_party():
	start_event("where_party")
	Tavern.change_image("Party")
	step()
	rogueAudio.play_sound("rogue_where_party_0")
	say("rogue", "I know we’d just gotten a bunch of treasure, but I like to increase those numbers when I can. So, I was off playing some cards at that table on the left. Those gentlemen didn’t know what hit them!")
	step()
	rogueAudio.play_sound("rogue_where_party_1")
	say("rogue", "Mind you, that’s not surprising given that they’re not the most observant of folk. But I’m not too concerned. A challenge is fun, but I won’t say no to an easy mark.")
	step()
	
	jump("", "", "where_party_menu")
	
	end_event()

func where_party_menu():
	start_event("where_party_menu")
	
	step()
	if Settings.get("accessibility"):
		rogueAudio.play_sounds(["1", "rogue_where_party_option_1", "2", "rogue_where_party_option_2", "3", "rogue_where_party_option_3"])
	say(null, """1. Don't you think that's a waste of your time? Anyway, did you notice anything else?
2. They may not be the most observant of folk, but I suspect you are. Did you notice anything else?
3. That’s a long time to stay in one place. Are you sure you didn’t go anywhere else?""")
	var choice = menu([
		["1", "persuasion_fail", {}],
		["2", "persuasion", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_party_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("rogue_dead"):
			jump("", "", "where_party_persuasion")
		else:
			Rakugo.store.set("rogue_dead", true)
			jump("", "", "where_party_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_party_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_party_persuasion_fail():
	start_event("where_party_persuasion_fail")
	
	step()
	rogueAudio.play_sound("rogue_persuasion_fail_0")
	say("rogue", "Are we done yet? I'd like to go skewer some rats now. If you won't leave, I'll just have to practice my skills on you.")
	step()
	ch.name = "Rogue (Ghost)"
	rogueAudio.play_sound("rogue_death")
	say(null, "She drew a rapier, but before she could so much as make a single swipe at me, a rat scurried up her leg and knocked the sword out of her hand. The rat then lunged for her jugular and bit deep, forming a fatal wound. A ghost rose from the pool of her blood.")
	step()
	rogueAudio.play_sound("rogue_persuasion_fail_1")
	say("rogue", "Oh, great. Still here. Probably some lame thing like I can't rest until the mystery is solved. Ugh. I guess I'm stuck answering your questions then.")
	step()
	jump("", "", "where_party_persuasion")
	
	end_event()

func where_party_persuasion():
	start_event("where_party_persuasion")
	
	step()
	rogueAudio.play_sound("rogue_where_party_persuasion_0")
	say("rogue", "The bard was singing all night, and other people came and went, but I don’t remember who. It didn’t seem very important at the time.")
	step()
	rogueAudio.play_sound("rogue_where_party_persuasion_1")
	say("rogue", "Oh, I do remember that it was the sorcerer over with the bard when the table caught on fire. The sorcerer does have a knack for that kind of thing. It was quite handy for me actually, it put my opponents off their game so I won even better.")
	step()
	rogueAudio.play_sound("rogue_where_party_persuasion_2")
	say("rogue", "Also, the paladin was having a conversation with the cleric. I couldn’t quite make it out from where I was, but given the whispers and the body language, it seemed to be about something pretty serious.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_party_insight():
	start_event("where_party_insight")
	
	step()
	rogueAudio.play_sound("rogue_where_party_insight_0")
	say("rogue", "Okay, fine, there’s a bit more I didn’t share. Right at the beginning, I went to pickpocket some of the paladin’s gold. I’m usually pretty good at that stuff, but I guess I was just unlucky because she totally noticed.")
	step()
	rogueAudio.play_sound("rogue_where_party_insight_1")
	say("rogue", "She got really mad, and she stormed off to talk to the cleric. Then I went to the gambling table.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_night():
	start_event("where_night")
	Tavern.change_image("Bedroom")
	step()
	rogueAudio.play_sound("rogue_where_night_0")
	say("rogue", "I was hoping to improve my skills, so I was wandering around practicing my sneak attack on the rats. I know, it’s not the most glamorous, but if I can bring proof I killed them, the barkeep will give me a few extra coin.")
	step()
	rogueAudio.play_sound("rogue_where_night_1")
	say("rogue", "Of course, I did need to sleep, so eventually I went to bed. I was in the second room on the left.")
	step()
	
	jump("", "", "where_night_menu")
	
	end_event()

func where_night_menu():
	start_event("where_night_menu")
	
	step()
	if Settings.get("accessibility"):
		rogueAudio.play_sounds(["1", "rogue_where_night_option_1", "2", "rogue_where_night_option_2", "3", "rogue_where_night_option_3"])
	say(null, """1. A very practical task. Did you notice anything interesting?
2. What a lame way to earn money! Did you notice anything interesting?
3. Do you really expect me to believe you spent hours killing rats?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "persuasion_fail", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_night_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("rogue_dead"):
			jump("", "", "where_night_persuasion")
		else:
			Rakugo.store.set("rogue_dead", true)
			jump("", "", "where_night_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_night_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_night_persuasion_fail():
	start_event("where_night_persuasion_fail")
	
	step()
	rogueAudio.play_sound("rogue_persuasion_fail_0")
	say("rogue", "Are we done yet? I'd like to go skewer some more rats now. If you won't leave, I'll just have to practice my skills on you.")
	step()
	ch.name = "Rogue (Ghost)"
	rogueAudio.play_sound("rogue_death")
	say(null, "She drew a rapier, but before she could so much as make a single swipe at me, a rat scurried up her leg and knocked the sword out of her hand. The rat then lunged for her jugular and bit deep, forming a fatal wound. A ghost rose from the pool of her blood.")
	step()
	rogueAudio.play_sound("rogue_persuasion_fail_1")
	say("rogue", "Oh, great. Still here. Probably some lame thing like I can't rest until the mystery is solved. Ugh. I guess I'm stuck answering your questions then.")
	step()
	jump("", "", "where_night_persuasion")
	
	end_event()

func where_night_persuasion():
	start_event("where_night_persuasion")
	
	step()
	rogueAudio.play_sound("rogue_where_night_persuasion_0")
	say("rogue", "I heard a weird noise coming from the right, but I think that was just the bard practicing another song. It’s hard to tell sometimes.")
	step()
	rogueAudio.play_sound("rogue_where_night_persuasion_1")
	say("rogue", "There were a few more rats than I was expecting. One in particular was kinda weird. It came from the left and made a beeline for the right.")
	step()
	rogueAudio.play_sound("rogue_where_night_persuasion_2")
	say("rogue", "Rats don’t usually look that determined, but I figured maybe it had smelled some really good cheese or something. I just focused my attention on the remaining rats.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_night_insight():
	start_event("where_night_insight")
	
	step()
	rogueAudio.play_sound("rogue_where_night_insight_0")
	say("rogue", "Well, I did kill some of them with traps rather than with my rapier. But yes, I was busy killing rats.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_amulet():
	start_event("where_amulet")
	Tavern.change_image("Treasure")
	step()
	rogueAudio.play_sound("rogue_where_amulet_0")
	say("rogue", "Right up front, of course. That’s the proper place for a scout. I’d just disarmed the trap on the chest, and the cleric told me she wanted to open it.")
	step()
	rogueAudio.play_sound("rogue_where_amulet_1")
	say("rogue", "She reckoned if there was anything else lurking in there she could take a few hits, and it had been a while since she’d gotten to open something. I said fair enough, you’re the boss after all.")
	step()
	rogueAudio.play_sound("rogue_where_amulet_2")
	say("rogue", "Kinda regretted it though when I saw the glint of something very shiny peeking out. She put it away real quick, though. I didn’t get a good look.")
	step()
	
	jump("", "", "about_where")
	end_event()

func about_amulet():
	start_event("about_amulet")
	Tavern.change_image("Amulet")
	rogueAudio.play_sound("rogue_amulet")
	say("rogue", "Yeah, that amulet does seem interesting. What do you want to know?")
	var choice = menu([
		["What do you know about the amulet?", "know", {}],
		["What would you do if you had the amulet?", "motive", {}],
		["What was it like when the amulet was discovered?", "discovered", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "know"):
		jump("", "", "amulet_know")
	elif cond(choice == "motive"):
		jump("", "", "amulet_motive")
	elif cond(choice == "discovered"):
		jump("", "", "amulet_discovered")
	end_event()

func amulet_know():
	start_event("amulet_know")
	
	step()
	rogueAudio.play_sound("rogue_amulet_know_0")
	say("rogue", "Sorry, that’s not really my area of expertise. All I can tell you is that cursed treasure is a very common occurrence.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_menu():
	start_event("amulet_know_menu")
	
	step()
	if Settings.get("accessibility"):
		rogueAudio.play_sounds(["1", "rogue_amulet_know_option_1", "2", "rogue_amulet_know_option_2"])
	say(null, """1. You look at a lot of treasure, I’m sure you have some knowledge of amulets that could help you here.
2. You sure you don’t know anything more?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "amulet_know_persuasion")
	elif cond(choice == "insight"):
		jump("", "", "amulet_know_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_know_persuasion():
	start_event("amulet_know_persuasion")
	
	step()
	rogueAudio.play_sound("rogue_amulet_know_persuasion_0")
	say("rogue", "Look, I already told you I don’t know a lot about this. I guess I can add that generally you need to be wearing an amulet for its powers to work. But I thought that’s common knowledge.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_insight():
	start_event("amulet_know_insight")
	
	step()
	rogueAudio.play_sound("rogue_amulet_know_insight_0")
	say("rogue", "Yes, I’m sure. If you want to know so bad, you should ask someone who actually understands these things. I’d recommend the wizard.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_motive():
	start_event("amulet_motive")
	
	step()
	rogueAudio.play_sound("rogue_amulet_motive_0")
	say("rogue", "Protection, huh? Could have used that a few years ago. All the people I’d care to protect are gone now. Some villain came through and slaughtered saying something about how it “builds character” and was “necessary for backstory development”. What a jerk.")
	step()
	rogueAudio.play_sound("rogue_amulet_motive_1")
	say("rogue", "No, I think the most use this thing has to me now is in its monetary value. I’m sure I could get a pretty penny for it.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_motive_menu():
	start_event("amulet_motive_menu")
	
	step()
	if Settings.get("accessibility"):
		rogueAudio.play_sounds(["1", "rogue_amulet_motive_option_1"])
	say(null, """1. Are you sure there are no more ulterior motives? Nothing else you’d use the amulet for?""")
	var choice = menu([
		["1", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "insight"):
		jump("", "", "amulet_motive_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_motive_insight():
	start_event("amulet_motive_insight")
	
	step()
	rogueAudio.play_sound("rogue_amulet_motive_insight_0")
	say("rogue", "I suppose I could use it for protecting people in this group, they’re basically the only people I care about right now. But I think I still care about money more. So yeah, I’d probably sell it.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_discovered():
	start_event("amulet_discovered")
	
	step()
	rogueAudio.play_sound("rogue_amulet_discovered_0")
	say("rogue", "Maybe I’m just imagining things, but the cleric looked a little... concerned? I mean, we hadn’t done any studying at that point, right?")
	step()
	rogueAudio.play_sound("rogue_amulet_discovered_1")
	say("rogue", "But she hurled it into her bag like she didn’t dare touch it any longer. Oh right, she wrapped it up in cloth too. Kinda weird that she was wearing it later then.")
	step()
	
	jump("", "", "about_amulet")
	end_event()

extends Dialogue

onready var bard_ch = Rakugo.define_character("Bard", "bard", Color("#44AA99"))
onready var cleric_ch = Rakugo.define_character("Cleric", "cleric", Color("#AA4499"))
onready var druid_ch = Rakugo.define_character("Druid", "druid", Color("#117733"))
onready var paladin_ch = Rakugo.define_character("Paladin", "paladin", Color("#DDCC77"))
onready var rogue_ch = Rakugo.define_character("Rogue", "rogue", Color("#332288"))
onready var sorcerer_ch = Rakugo.define_character("Sorcerer", "sorcerer", Color("#882255"))
onready var wizard_ch = Rakugo.define_character("Wizard", "wizard", Color("#88CCEE"))

onready var audioPlayer = get_parent().get_node("CharacterChooseAudio")

func first_dialogue():
	start_event("first_dialogue")
	audioPlayer.play_sound("cleric_intro_0")
	say("cleric", "Congratulations, everyone! Great job with this last adventure.")
	step()
	audioPlayer.play_sound("bard_intro_0")
	say("bard", "Y'all did marvelous!")
	step()
	audioPlayer.play_sound("rogue_intro_0")
	say("rogue", "What about the treasure?")
	step()
	audioPlayer.play_sound("paladin_intro_0")
	say("paladin", "Of course someone had to ruin the mood with their greed.")
	step()
	audioPlayer.play_sound("sorcerer_intro_0")
	say("sorcerer", "Aren't you even a little bit curious?")
	step()
	audioPlayer.play_sound("wizard_intro_0")
	say("wizard", "I must admit, my scholarly interest was piqued.")
	step()
	audioPlayer.play_sound("druid_intro_0")
	say("druid", "Hey, guys, can you be quiet? It looks like she wants to say something!")
	step()
	audioPlayer.play_sound("cleric_intro_1")
	say("cleric", "Thank you. As you know, our policy is to wait to divide the loot until the day after the adventure, so we are all well rested and thinking clearly.")
	step()
	audioPlayer.play_sound("cleric_intro_2")
	say("cleric", "But I can see the curiosity is driving some of you mad, so against my better judgement I'll tell you about what we found.")
	step()
	Tavern.change_image("Amulet")
	audioPlayer.play_sound("cleric_intro_3")
	say("cleric", "This amulet I have here is an amulet of protection. I'm not entirely sure how it works, but the wizard has taken some traces and will report on her studies.")
	step()
	Tavern.change_image("Tavern")
	audioPlayer.play_sound("bard_intro_1")
	say("bard", "In the meantime, let's party!")
	step()
	audioPlayer.play_sound("player_intro_0")
	say(null, "What an interesting group! I wonder what the party will be like.")
	step()
	Tavern.change_image("Bedroom")
	audioPlayer.play_sound("player_intro_1")
	say(null, "Several hours later...")
	step()
	audioPlayer.play_sound("player_intro_2")
	say(null, "What a night! I don't really remember it, but I'm pretty sure that means it was a lot of fun.")
	step()
	Tavern.change_image("Tavern")
	audioPlayer.play_sound("cleric_intro_4")
	say("cleric", "Good morning, everyone! I have some bad news for you. The amulet is missing. It seems to have been stolen!")
	step()
	audioPlayer.play_sound("wizard_intro_1")
	say("wizard", "I hate to add on to the bad news, but... it seems this amulet is also cursed. I don't fully understand it, but it seems the curse was triggered by the theft.")
	step()
	audioPlayer.play_sound("paladin_intro_1")
	say("paladin", "For the sake of simplicity, let us say the thief was one of us. We need an impartial investigator.")
	step()
	audioPlayer.play_sound("player_intro_3")
	say(null, "Um, I could do that!")
	step()
	audioPlayer.play_sound("cleric_intro_5")
	say("cleric", "Alright, go ahead.")
	jump("", "", "select_character")
	end_event()

func select_character():
	start_event("select_character")
	audioPlayer.play_sound("choose_character")
	say(null, "Who should I talk to?")
	var choice = menu([
		["Bard", "bard", {}],
		["Cleric", "cleric", {}],
		["Druid", "druid", {}],
		["Paladin", "paladin", {}],
		["Rogue", "rogue", {}],
		["Sorcerer", "sorcerer", {}],
		["Wizard", "wizard", {}],
		["Make my accusation", "accusation", {}]
	])
	step()
	Music.play_detective()
	if cond(choice == "bard"):
		jump("Bard", "", "")
	elif cond(choice == "cleric"):
		jump("Cleric", "", "")
	elif cond(choice == "druid"):
		jump("Druid", "", "")
	elif cond(choice == "paladin"):
		jump("Paladin", "", "")
	elif cond(choice == "rogue"):
		jump("Rogue", "", "")
	elif cond(choice == "sorcerer"):
		jump("Sorcerer", "", "")
	elif cond(choice == "wizard"):
		jump("Wizard", "", "")
	else:
		say(null, "Is it really time? I should make sure I've talked to everyone first.")
		var accuse_choice = menu([
			["Yes, make an accusation", "confirm", {}],
			["No, go back to talking to characters", "return", {}]
		])
		step()
		if cond(accuse_choice == "confirm"):
			Music.play_detective()
			jump("FinalDialogue", "", "")
		else:
			jump("", "", "select_character")
	end_event()

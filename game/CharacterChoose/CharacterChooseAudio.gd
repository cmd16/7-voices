extends AudioStreamPlayer

const sounds = {
	"cleric_intro_0": preload("res://assets/audio/va/cleric_intro_0.wav"),
	"cleric_intro_1": preload("res://assets/audio/va/cleric_intro_1.wav"),
	"cleric_intro_2": preload("res://assets/audio/va/cleric_intro_2.wav"),
	"cleric_intro_3": preload("res://assets/audio/va/cleric_intro_3.wav"),
	"cleric_intro_4": preload("res://assets/audio/va/cleric_intro_4.wav"),
	"druid_intro_0": preload("res://assets/audio/va/druid_intro_0.wav"),
	"paladin_intro_0": preload("res://assets/audio/va/paladin_intro_0.wav"),
	"paladin_intro_1": preload("res://assets/audio/va/paladin_intro_1.wav"),
	"rogue_intro_0": preload("res://assets/audio/va/rogue_intro_0.wav"),
	"sorcerer_intro_0": preload("res://assets/audio/va/sorcerer_intro_0.wav"),
	"wizard_intro_0": preload("res://assets/audio/va/wizard_intro_0.wav"),
	"wizard_intro_1": preload("res://assets/audio/va/wizard_intro_1.wav"),
	"player_intro_0": preload("res://assets/audio/va/accessibility/player_intro_0.wav"),
	"player_intro_1": preload("res://assets/audio/va/accessibility/player_intro_1.wav"),
	"player_intro_2": preload("res://assets/audio/va/accessibility/player_intro_2.wav"),
	"player_intro_3": preload("res://assets/audio/va/accessibility/player_intro_3.wav"),
	"choose_character": preload("res://assets/audio/va/accessibility/choose_character.wav"),
	"bard_intro_0": preload("res://assets/audio/va/bard_intro_0.wav"),
	"bard_intro_1": preload("res://assets/audio/va/bard_intro_1.wav")
}

func play_sound(sound_name):
	print(self.name, " playing sound: ", sound_name)
	if !self.sounds.has(sound_name):
		print(self.name, " skipping sound: ", sound_name)
		return
	self.stream = self.sounds[sound_name]
	self.play()

func play_sounds(sounds):
	for sound in sounds:
		self.play_sound(sound)
		yield(self, "finished")

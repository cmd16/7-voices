extends Dialogue

onready var ch = Rakugo.define_character("Paladin" if !Rakugo.store.get("paladin_dead") else "Paladin (Ghost)", "paladin", Color("#DDCC77"))
onready var paladinAudio = get_node("../PaladinAudio")

"""
About you
	What are you good at?
	Why are you with this group?
About your whereabouts
	Where were you during the party?
	Where were you last night?
	Where were you when the amulet was discovered?
About the amulet
	What do you know about the amulet?
	What would you do if you had the amulet?
	What was it like when the amulet was discovered?
"""

func first_dialogue():
	start_event("first_dialogue")
	Tavern.change_image("Tavern")
	paladinAudio.play_sound("paladin_greeting")
	say("paladin", "Hello. Thank you for aiding our investigation.")
	
	step()
	paladinAudio.play_sound("paladin_main")
	say("paladin", "What is your inquiry?")
	
	var start_topic = menu([
		["About you", "you", {}],
		["About your whereabouts", "where", {}],
		["About the amulet", "amulet", {}],
		["No more questions.", "done", {}]
	])
	step()
	
	if cond(start_topic == "you"):
		jump("", "", "about_you")
	elif cond(start_topic == "where"):
		jump("", "", "about_where")
	elif cond(start_topic == "amulet"):
		jump("", "", "about_amulet")
	else:
		Music.play_tavern()
		jump("CharacterChoose", "", "select_character")
	
	end_event()

func about_you():
	start_event("about_you")
	Tavern.change_image("Question")
	paladinAudio.play_sound("paladin_you")
	say("paladin", "Of course. I have nothing to hide.")
	var choice = menu([
			["What are you good at?", "skills", {}],
			["Why are you with this group?", "group", {}],
			["Never mind.", "back", {}]
		])
	step()

	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "skills"):
		jump("", "", "you_skills")
	elif cond(choice == "group"):
		jump("", "", "you_group")
	end_event()

func you_skills():
	start_event("you_skills")
	
	step()
	paladinAudio.play_sound("paladin_you_skills_0")
	say("paladin", "As you might have guessed from the heavy armor, I’m quite tough. Many attacks are directed to me but fail to hit, and as for the rest of the attacks on me, I can take many hits before I start really feeling hurt.")
	step()
	paladinAudio.play_sound("paladin_you_skills_1")
	say("paladin", "I have some healing as well, which can be useful. And I do sometimes enter into negotiations, particularly when it is important to have someone respectable. I am the most upstanding member of the group, after all.")
	step()
	
	jump("", "", "about_you")
	end_event()

func you_group():
	start_event("you_group")
	
	step()
	paladinAudio.play_sound("paladin_you_group_0")
	say("paladin", "This band of ruffians had been making trouble for quite some time and I was tired of having to deal with their... pardon my language... shenanigans.")
	step()
	paladinAudio.play_sound("paladin_you_group_1")
	say("paladin", "I figured it was time for an inside job. Maybe if I joined there would be some hope of a moral compass. Not to say that all of them are terrible, but let’s just say it doesn’t surprise me that we have someone in our midst who’s willing to steal from the group.")
	step()

	jump("", "", "about_you")
	end_event()

func about_where():
	start_event("about_where")
	Tavern.change_image("Tavern")
	paladinAudio.play_sound("paladin_where")
	say("paladin", "For what time period shall I discuss my whereabouts?")
	var choice = menu([
		["Where were you when the amulet was discovered?", "amulet", {}],
		["Where were you during the party?", "party", {}],
		["Where were you after the party?", "night", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "party"):
		jump("", "", "where_party")
	elif cond(choice == "night"):
		jump("", "", "where_night")
	elif cond(choice == "amulet"):
		jump("", "", "where_amulet")
	
	end_event()

func where_party():
	start_event("where_party")
	Tavern.change_image("Party")
	step()
	paladinAudio.play_sound("paladin_where_party_0")
	say("paladin", "I was talking with the cleric. We had a lot of good discussion that evening. Mostly about improving our combat strategy, though we did have a brief aside about morals. Especially given that the rogue had just tried to steal from me! What a disappointment.")
	step()
	
	jump("", "", "where_party_menu")
	
	end_event()

func where_party_menu():
	start_event("where_party_menu")
	
	step()
	if Settings.get("accessibility"):
		paladinAudio.play_sounds(["1", "paladin_where_party_option_1", "2", "paladin_where_party_option_2", "3", "paladin_where_party_option_3"])
	say(null, """1. A clandestine discussion doesn't sound very responsible. Can you share waht you said about morals?
2. I’m glad you’re able to have those important discussions. Can you share what you said about morals?
3. Did you discuss anything else?""")
	var choice = menu([
		["1", "persuasion_fail", {}],
		["2", "persuasion", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_party_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("paladin_dead"):
			jump("", "", "where_party_persuasion")
		else:
			Rakugo.store.set("paladin_dead", true)
			jump("", "", "where_party_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_party_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_party_persuasion_fail():
	start_event("where_party_persuasion_fail")
	
	step()
	paladinAudio.play_sound("paladin_persuasion_fail_0")
	say("paladin", "I'm disappointed with your incompetence. You still don't know the culprit yet, do you? I wonder if a little corporal punishment would improve your skills.")
	step()
	ch.name = "Paladin (Ghost)"
	paladinAudio.play_sound("paladin_death")
	say(null, "As I considered just what physical torment the paladin had in store for me, I noticed a strange sight. The paladin's sword rose up of its own accord and stabbed her clean through the heart. Then the sword traced an outline of her, and she became a ghost.")
	step()
	paladinAudio.play_sound("paladin_persuasion_fail_1")
	say("paladin", "I may be a ghost, but I'm still committed to help.")
	step()
	jump("", "", "where_party_persuasion")
	
	end_event()

func where_party_persuasion():
	start_event("where_party_persuasion")
	
	step()
	paladinAudio.play_sound("paladin_where_party_persuasion_0")
	say("paladin", "To put it bluntly, we were complaining about some of the other members.")
	step()
	paladinAudio.play_sound("paladin_where_party_persuasion_1")
	say("paladin", "The rogue has been stealing quite a lot lately, sometimes to the point of distracting us from our current mission. The sorcerer keeps causing collateral damage and she hasn’t shown nearly enough remorse. The bard is sometimes so busy doting on the wizard that she forgets her duties.")
	step()
	paladinAudio.play_sound("paladin_where_party_persuasion_2")
	say("paladin", "And it generally takes the group a little too long to respond to orders. They just don’t have fear of authority anymore. A real shame.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_party_insight():
	start_event("where_party_insight")
	
	step()
	paladinAudio.play_sound("paladin_where_party_insight_0")
	say("paladin", "I didn’t want to get into this, but I suppose if it could help the investigation then I must.")
	step()
	paladinAudio.play_sound("paladin_where_party_insight_1")
	say("paladin", "We also discussed the amulet. We were concerned certain party members might use it for selfish purposes or simply not think about the consequences.")
	step()
	paladinAudio.play_sound("paladin_where_party_insight_2")
	say("paladin", "I proposed we create some sort of test to figure out who might be fit to hold onto this. The cleric told me she had something in mind. She did not clarify.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_night():
	start_event("where_night")
	Tavern.change_image("Bedroom")
	step()
	paladinAudio.play_sound("paladin_where_night_0")
	say("paladin", "I went to sleep in the first room on the left. Then I got up early and spent a few hours keeping watch.")
	step()
	paladinAudio.play_sound("paladin_where_night_1")
	say("paladin", "This group seems to think they can just stay up late and then nothing bad will happen while they sleep, so I thought I ought to balance it out by getting up early for my watch.")
	step()
	
	jump("", "", "where_night_menu")
	
	end_event()

func where_night_menu():
	start_event("where_night_menu")
	
	step()
	if Settings.get("accessibility"):
		paladinAudio.play_sounds(["1", "paladin_where_night_option_1", "2", "paladin_where_night_option_2", "3", "paladin_where_night_option_3"])
	say(null, """1. A carefully thought out strategy. Did you notice anything?
2. Sounds to me like nobody has a watch in the middle of the night. Did you notice anything?
3. Why were you on watch tonight? Did you have a heightened level of concern?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "persuasion_fail", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_night_persuasion")
	elif cond(choice == "persuasion"):
		if Rakugo.store.get("paladin_dead"):
			jump("", "", "where_night_persuasion")
		else:
			Rakugo.store.set("paladin_dead", true)
			jump("", "", "where_night_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_night_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_night_persuasion_fail():
	start_event("where_night_persuasion_fail")
	
	step()
	paladinAudio.play_sound("paladin_persuasion_fail_0")
	say("paladin", "I'm disappointed with your incompetence. You still don't know the culprit yet, do you? I wonder if a little corporal punishment would improve your skills.")
	step()
	ch.name = "Paladin (Ghost)"
	paladinAudio.play_sound("paladin_death")
	say(null, "As I considered just what physical torment the paladin had in store for me, I noticed a strange sight. The paladin's sword rose up of its own accord and stabbed her clean through the heart. Then the sword traced an outline of her, and she became a ghost.")
	step()
	paladinAudio.play_sound("paladin_persuasion_fail_1")
	say("paladin", "I may be a ghost, but I'm still committed to help.")
	step()
	jump("", "", "where_night_persuasion")
	
	end_event()

func where_night_persuasion():
	start_event("where_night_persuasion")
	
	step()
	paladinAudio.play_sound("paladin_where_night_persuasion_0")
	say("paladin", "No, unfortunately. If anything of interest happened last night, it must have happened before I got up.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_night_insight():
	start_event("where_night_insight")
	
	step()
	paladinAudio.play_sound("paladin_where_night_insight_0")
	say("paladin", "I always like to keep watch the night before loot division. This was no different. Except for the fact that there actually was a theft. That doesn’t usually happen.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_amulet():
	start_event("where_amulet")
	Tavern.change_image("Treasure")
	step()
	paladinAudio.play_sound("paladin_where_amulet_0")
	say("paladin", "I was fairly close to the front of the group, so I had a decent view. I know the rogue was nearby, watching with those greedy eyes as the cleric opened the chest.")
	step()
	paladinAudio.play_sound("paladin_where_amulet_1")
	say("paladin", "The scene was brief, though. The cleric pulled something out that I presume was the amulet, then she quickly stuffed it into her bag.")
	step()
	
	jump("", "", "about_where")
	end_event()

func about_amulet():
	start_event("about_amulet")
	Tavern.change_image("Amulet")
	paladinAudio.play_sound("paladin_amulet")
	say("paladin", "The object we've all been thinking about lately. What would you like to know?")
	var choice = menu([
		["What do you know about the amulet?", "know", {}],
		["What would you do if you had the amulet?", "motive", {}],
		["What was it like when the amulet was discovered?", "discovered", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "know"):
		jump("", "", "amulet_know")
	elif cond(choice == "motive"):
		jump("", "", "amulet_motive")
	elif cond(choice == "discovered"):
		jump("", "", "amulet_discovered")
	end_event()

func amulet_know():
	start_event("amulet_know")
	
	step()
	paladinAudio.play_sound("paladin_amulet_know_0")
	say("paladin", "It’s an amulet that can protect people. Maybe multiple people? I’m not certain.")
	step()
	paladinAudio.play_sound("paladin_amulet_know_1")
	say("paladin", "What I am certain about is that this amulet carries a great power and thus must be wielded with a great responsibility. Unfortunately, it turned out to be this group that ended up finding it.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_menu():
	start_event("amulet_know_menu")
	
	step()
	if Settings.get("accessibility"):
		paladinAudio.play_sounds(["1", "paladin_amulet_know_option_1", "2", "paladin_amulet_know_option_2"])
	say(null, """1. What do you mean, “unfortunately”?
2. Are you sure you don’t know more about how many people it can protect?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "amulet_know_persuasion")
	elif cond(choice == "insight"):
		jump("", "", "amulet_know_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_know_persuasion():
	start_event("amulet_know_persuasion")
	
	step()
	paladinAudio.play_sound("paladin_amulet_know_persuasion_0")
	say("paladin", "I don’t trust many of my companions. Who knows what chaos they could cause with this thing?")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_insight():
	start_event("amulet_know_insight")
	
	step()
	paladinAudio.play_sound("paladin_amulet_know_insight_0")
	say("paladin", "I know I have kinds of protection that can extend to a limited area rather than a limited number of people. It’s possible the amulet could be the same deal: affecting a certain area.")
	step()
	paladinAudio.play_sound("paladin_amulet_know_insight_1")
	say("paladin", "I suppose I’m hoping for that, as that could potentially protect a larger group of people. But I don’t know for sure how this works.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_motive():
	start_event("amulet_motive")
	
	step()
	paladinAudio.play_sound("paladin_amulet_motive_0")
	say("paladin", "The only appropriate thing to do with an item such as this: serve the community. Any town or village has those who are marginalized by society, and much as we might try to rectify that with our adventuring (or make it worse, in the case of some people in our group), the fact is that a little bit of magical protection could go a long way.")
	step()
	
	jump("", "", "amulet_motive_menu")

	end_event()

func amulet_motive_menu():
	start_event("amulet_motive_menu")
	
	step()
	if Settings.get("accessibility"):
		paladinAudio.play_sounds(["1", "paladin_amulet_motive_option_1"])
	say(null, """1. So many possibilities. Are you sure that’s who you’d use the amulet for?""")
	var choice = menu([
		["1", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "insight"):
		jump("", "", "amulet_motive_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_motive_insight():
	start_event("amulet_motive_insight")
	
	step()
	paladinAudio.play_sound("paladin_amulet_motive_insight_0")
	say("paladin", "Excuse you, I am an upstanding member of society! I’m very disappointed that you would question my convictions. And yes, I am certain that I would use the amulet to serve the community.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_discovered():
	start_event("amulet_discovered")
	
	step()
	paladinAudio.play_sound("paladin_amulet_discovered_0")
	say("paladin", "Oh, it was dreadful! The ruckus reminded me of all the other times I wish I hadn’t cast my lot with this deplorable band of ruffians.")
	step()
	paladinAudio.play_sound("paladin_amulet_discovered_1")
	say("paladin", "Even though many people hadn’t gotten a good look at the item, they saw the way the cleric put it away and knew we had something valuable, and so they all began squabbling and squawking like a bunch of parrots. “Mine!” “No, mine!”")
	step()
	paladinAudio.play_sound("paladin_amulet_discovered_2")
	say("paladin", "I demanded they stop and told them that if all that nonsense hadn’t summoned more monsters then I would punish them all myself. Much as I hate to use that tactic, brandishing a sword is quite effective.")
	step()
	paladinAudio.play_sound("paladin_amulet_discovered_3")
	say("paladin", "To be fair, I suppose that’s why the rogue is usually the one to grab the treasure: she may be greedy, but she knows how to be discreet so that nobody knows what we have until it’s loot distribution time.")
	step()
	paladinAudio.play_sound("paladin_amulet_discovered_4")
	say("paladin", "I can’t believe I actually praised the rogue for something! Tell no-one about this.")
	step()
	
	jump("", "", "about_amulet")
	end_event()

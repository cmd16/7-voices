extends AudioStreamPlayer

const sounds = {
	"paladin_amulet": preload("res://assets/audio/va/paladin_amulet.wav"),
	"paladin_amulet_discovered_0": preload("res://assets/audio/va/paladin_amulet_discovered_0.wav"),
	"paladin_amulet_discovered_1": preload("res://assets/audio/va/paladin_amulet_discovered_1.wav"),
	"paladin_amulet_discovered_2": preload("res://assets/audio/va/paladin_amulet_discovered_2.wav"),
	"paladin_amulet_discovered_3": preload("res://assets/audio/va/paladin_amulet_discovered_3.wav"),
	"paladin_amulet_discovered_4": preload("res://assets/audio/va/paladin_amulet_discovered_4.wav"),
	"paladin_amulet_know_0": preload("res://assets/audio/va/paladin_amulet_know_0.wav"),
	"paladin_amulet_know_1": preload("res://assets/audio/va/paladin_amulet_know_1.wav"),
	"paladin_amulet_know_insight_0": preload("res://assets/audio/va/paladin_amulet_know_insight_0.wav"),
	"paladin_amulet_know_insight_1": preload("res://assets/audio/va/paladin_amulet_know_insight_1.wav"),
	"paladin_amulet_know_persuasion_0": preload("res://assets/audio/va/paladin_amulet_know_persuasion_0.wav"),
	"paladin_amulet_motive_0": preload("res://assets/audio/va/paladin_amulet_motive_0.wav"),
	"paladin_amulet_motive_insight_0": preload("res://assets/audio/va/paladin_amulet_motive_insight_0.wav"),
	"paladin_greeting": preload("res://assets/audio/va/paladin_greeting.wav"),
	"paladin_intro_0": preload("res://assets/audio/va/paladin_intro_0.wav"),
	"paladin_intro_1": preload("res://assets/audio/va/paladin_intro_1.wav"),
	"paladin_main": preload("res://assets/audio/va/paladin_main.wav"),
	"paladin_where": preload("res://assets/audio/va/paladin_where.wav"),
	"paladin_where_amulet_0": preload("res://assets/audio/va/paladin_where_amulet_0.wav"),
	"paladin_where_amulet_1": preload("res://assets/audio/va/paladin_where_amulet_1.wav"),
	"paladin_where_night_0": preload("res://assets/audio/va/paladin_where_night_0.wav"),
	"paladin_where_night_1": preload("res://assets/audio/va/paladin_where_night_1.wav"),
	"paladin_where_night_insight_0": preload("res://assets/audio/va/paladin_where_night_insight_0.wav"),
	"paladin_where_night_persuasion_0": preload("res://assets/audio/va/paladin_where_night_persuasion_0.wav"),
	"paladin_where_party_0": preload("res://assets/audio/va/paladin_where_party_0.wav"),
	"paladin_where_party_insight_0": preload("res://assets/audio/va/paladin_where_party_insight_0.wav"),
	"paladin_where_party_insight_1": preload("res://assets/audio/va/paladin_where_party_insight_1.wav"),
	"paladin_where_party_insight_2": preload("res://assets/audio/va/paladin_where_party_insight_2.wav"),
	"paladin_where_party_persuasion_0": preload("res://assets/audio/va/paladin_where_party_persuasion_0.wav"),
	"paladin_where_party_persuasion_1": preload("res://assets/audio/va/paladin_where_party_persuasion_1.wav"),
	"paladin_where_party_persuasion_2": preload("res://assets/audio/va/paladin_where_party_persuasion_2.wav"),
	"paladin_you": preload("res://assets/audio/va/paladin_you.wav"),
	"paladin_you_group_0": preload("res://assets/audio/va/paladin_you_group_0.wav"),
	"paladin_you_group_1": preload("res://assets/audio/va/paladin_you_group_1.wav"),
	"paladin_you_skills_0": preload("res://assets/audio/va/paladin_you_skills_0.wav"),
	"paladin_you_skills_1": preload("res://assets/audio/va/paladin_you_skills_1.wav"),
	"paladin_persuasion_fail_0": preload("res://assets/audio/va/paladin_persuasion_fail_0.wav"),
	"paladin_persuasion_fail_1": preload("res://assets/audio/va/paladin_persuasion_fail_1.wav"),
	"paladin_amulet_know_option_1": preload("res://assets/audio/va/accessibility/paladin_amulet_know_option_1.wav"),
	"paladin_amulet_know_option_2": preload("res://assets/audio/va/accessibility/paladin_amulet_know_option_2.wav"),
	"paladin_amulet_motive_option_1": preload("res://assets/audio/va/accessibility/paladin_amulet_motive_option_1.wav"),
	"paladin_death": preload("res://assets/audio/va/accessibility/paladin_death.wav"),
	"paladin_where_night_option_1": preload("res://assets/audio/va/accessibility/paladin_where_night_option_1.wav"),
	"paladin_where_night_option_2": preload("res://assets/audio/va/accessibility/paladin_where_night_option_2.wav"),
	"paladin_where_night_option_3": preload("res://assets/audio/va/accessibility/paladin_where_night_option_3.wav"),
	"paladin_where_party_option_1": preload("res://assets/audio/va/accessibility/paladin_where_party_option_1.wav"),
	"paladin_where_party_option_2": preload("res://assets/audio/va/accessibility/paladin_where_party_option_2.wav"),
	"paladin_where_party_option_3": preload("res://assets/audio/va/accessibility/paladin_where_party_option_3.wav"),
	"1": preload("res://assets/audio/va/accessibility/1.wav"),
	"2": preload("res://assets/audio/va/accessibility/2.wav"),
	"3": preload("res://assets/audio/va/accessibility/3.wav"),
}

func play_sound(sound_name):
	print(self.name, " playing sound: ", sound_name)
	if !self.sounds.has(sound_name):
		print(self.name, " skipping sound: ", sound_name)
		return
	self.stream = self.sounds[sound_name]
	self.play()

func play_sounds(sounds):
	for sound in sounds:
		self.play_sound(sound)
		yield(self, "finished")

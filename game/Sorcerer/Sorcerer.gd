extends Dialogue

onready var ch = Rakugo.define_character("Sorcerer" if !Rakugo.store.get("sorcerer_dead") else "Sorcerer (Ghost)", "sorcerer", Color("#882255"))
onready var sorcererAudio = get_node("../SorcererAudio")

"""
About you
	What are you good at?
	Why are you with this group?
About your whereabouts
	Where were you during the party?
	Where were you last night?
	Where were you when the amulet was discovered?
About the amulet
	What do you know about the amulet?
	What would you do if you had the amulet?
	What was it like when the amulet was discovered?
"""

func first_dialogue():
	start_event("first_dialogue")
	Tavern.change_image("Tavern")
	sorcererAudio.play_sound("sorcerer_greeting")
	say("sorcerer", "Hi! Good to see you.")
	
	step()
	
	sorcererAudio.play_sound("sorcerer_main")
	say("sorcerer", "What do you need to know?")
	
	var start_topic = menu([
		["About you", "you", {}],
		["About your whereabouts", "where", {}],
		["About the amulet", "amulet", {}],
		["No more questions.", "done", {}]
	])
	step()
	
	if cond(start_topic == "you"):
		jump("", "", "about_you")
	elif cond(start_topic == "where"):
		jump("", "", "about_where")
	elif cond(start_topic == "amulet"):
		jump("", "", "about_amulet")
	else:
		Music.play_tavern()
		jump("CharacterChoose", "", "select_character")
	
	end_event()

func about_you():
	start_event("about_you")
	Tavern.change_image("Question")
	sorcererAudio.play_sound("sorcerer_you")
	say("sorcerer", "Sure, I'm happy to talk about myself.")
	var choice = menu([
			["What are you good at?", "skills", {}],
			["Why are you with this group?", "group", {}],
			["Never mind.", "back", {}]
		])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "skills"):
		jump("", "", "you_skills")
	elif cond(choice == "group"):
		jump("", "", "you_group")
	end_event()

func you_skills():
	start_event("you_skills")
	
	step()
	sorcererAudio.play_sound("sorcerer_you_skills_0")
	say("sorcerer", "Magic, obviously! Especially the kind that’s good for showing off. But also the kind that’s a bit unpredictable. Given that, I’m quite good at causing a distraction, which is always a good bit of fun!")
	step()
	sorcererAudio.play_sound("sorcerer_you_skills_1")
	say("sorcerer", "Fire is also a good bit of fun, but I try not to burn too much down. I’ve been chased out of quite a few towns and I’m not hoping to repeat that experience.")
	step()
	
	jump("", "", "about_you")
	end_event()

func you_group():
	start_event("you_group")
	
	step()
	sorcererAudio.play_sound("sorcerer_you_group_0")
	say("sorcerer", "I dunno, I was getting a bit bored by myself honestly. Thought it was time to join a gang. And then I found a group that didn’t immediately kick me out and I said “perfect! This is the place for me.”")
	step()
	
	jump("", "", "about_you")
	end_event()

func about_where():
	start_event("about_where")
	Tavern.change_image("Tavern")
	sorcererAudio.play_sound("sorcerer_where")
	say("sorcerer", "I've wandered around quite a bit! What time are you asking about?")
	var choice = menu([
		["Where were you when the amulet was discovered?", "amulet", {}],
		["Where were you during the party?", "party", {}],
		["Where were you after the party?", "night", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "party"):
		jump("", "", "where_party")
	elif cond(choice == "night"):
		jump("", "", "where_night")
	elif cond(choice == "amulet"):
		jump("", "", "where_amulet")
	
	end_event()

func where_party():
	start_event("where_party")
	Tavern.change_image("Party")
	step()
	sorcererAudio.play_sound("sorcerer_where_party_0")
	say("sorcerer", "I was chatting with the cleric for a bit at the beginning, but then the paladin came by and told me to bugger off (not that she used those words, mind you, no she’s too proper for that kind of language), so I left.")
	step()
	sorcererAudio.play_sound("sorcerer_where_party_1")
	say("sorcerer", "That’s when I noticed the bard near the middle of the room. She was having a wonderful time singing and I decided to join in! It was all going great until I decided to add in a little magic to make a cool visual display along with the music.")
	step()
	sorcererAudio.play_sound("sorcerer_where_party_2")
	say("sorcerer", "The display worked just great, but then I also accidentally sent a few too many sparks out of my hands (rather nasty side effect, that one) and next thing you know the table was on fire! There was quite a bit of screaming from that.")
	step()
	sorcererAudio.play_sound("sorcerer_where_party_3")
	say("sorcerer", "I went to fetch some water to sort it out, and after that I decided I should probably leave before I caused any more trouble, so I went up to my room a bit early. Sounds like the party went on for quite a while after that.")
	step()
	
	jump("", "", "where_party_menu")
	
	end_event()

func where_party_menu():
	start_event("where_party_menu")
	
	step()
	if Settings.get("accessibility"):
		sorcererAudio.play_sounds(["1", "sorcerer_where_party_option_1", "2", "sorcerer_where_party_option_2", "3", "sorcerer_where_party_option_3"])
	say(null, """1. More causing trouble, huh? Did it look like the fire drew anyone else's attention?
2. Sounds like that fire was a pretty big deal. Did it look like it drew anyone else’s attention?
3. You went up to your room early? You didn’t go anywhere else?""")
	var choice = menu([
		["1", "persuasion_fail", {}],
		["2", "persuasion", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_party_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("sorcerer_dead"):
			jump("", "", "where_party_persuasion")
		else:
			Rakugo.store.set("sorcerer_dead", true)
			jump("", "", "where_party_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_party_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_party_persuasion_fail():
	start_event("where_party_persuasion_fail")
	
	step()
	sorcererAudio.play_sound("sorcerer_persuasion_fail_0")
	say("sorcerer", "I don't mean to cut this short, but I just have this feeling that my magic is about to flare up and I'd hate to hurt you.")
	step()
	ch.name = "Sorcerer (Ghost)"
	sorcererAudio.play_sound("sorcerer_death")
	say(null, "But the sorcerer's magic wasn't what flared up. There was a low crackle and then the sorcerer's entire body was ablaze like a small bonfire. She was charred to a crisp before anyone had a chance to react. The ashes rose up to form a ghost.")
	step()
	sorcererAudio.play_sound("sorcerer_persuasion_fail_1")
	say("sorcerer", "I wonder, can you burn a ghost? Let's not find out. Anyway, I was answering your question...")
	step()
	jump("", "", "where_party_persuasion")
	
	end_event()

func where_party_persuasion():
	start_event("where_party_persuasion")
	
	step()
	sorcererAudio.play_sound("sorcerer_where_party_persuasion_0")
	say("sorcerer", "It definitely felt like one of those “oh wow everybody is looking” moments! I can’t say who came over though, I left pretty quickly to go get water.")
	step()
	sorcererAudio.play_sound("sorcerer_where_party_persuasion_1")
	say("sorcerer", "Normally the druid just makes some water with her magic and that fixes it, but that didn’t happen this time. I think she was gone at the time, though I’m not sure where to.")
	step()
		
	jump("", "", "where_party_menu")
	end_event()

func where_party_insight():
	start_event("where_party_insight")
	
	step()
	sorcererAudio.play_sound("sorcerer_where_party_insight_0")
	say("sorcerer", "Well ok, I didn’t stay there the whole evening. I missed the party so I headed back down towards the end.")
	step()
	sorcererAudio.play_sound("sorcerer_where_party_insight_1")
	say("sorcerer", "I was a bit too nervous to join in the main activities this time though, so I just turned invisible and had fun wandering around the room unseen.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_night():
	start_event("where_night")
	Tavern.change_image("Bedroom")
	step()
	sorcererAudio.play_sound("sorcerer_where_night_0")
	say("sorcerer", "Well, I felt my desire to party hadn’t quite been satisfied yet, so I left the tavern for a nearby clearing. Practiced a bit of magic there, set off a few more accidental fires but it was fairly easy to put them out.")
	step()
	sorcererAudio.play_sound("sorcerer_where_night_1")
	say("sorcerer", "I did eventually get tired, so I went back to the tavern to sleep. I was in the second room on the right.")
	step()
	
	jump("", "", "where_night_menu")
	
	end_event()

func where_night_menu():
	start_event("where_night_menu")
	
	step()
	if Settings.get("accessibility"):
		sorcererAudio.play_sounds(["1", "sorcerer_where_night_option_1", "2", "sorcerer_where_night_option_2", "3", "sorcerer_where_night_option_3"])
	say(null, """1. Sounds like fun! Did you notice anyone else?
2. You better hope you didn't burn anyone's livestock. Did you notice anyone else?
3. Are you positive you didn’t do anything else?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "persuasion_fail", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_night_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("sorcerer_dead"):
			jump("", "", "where_night_persuasion")
		else:
			Rakugo.store.set("sorcerer_dead", true)
			jump("", "", "where_night_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_night_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_night_persuasion_fail():
	start_event("where_night_persuasion_fail")
	
	step()
	sorcererAudio.play_sound("sorcerer_persuasion_fail_0")
	say("sorcerer", "I don't mean to cut this short, but I just have this feeling that my magic is about to flare up and I'd hate to hurt you.")
	step()
	ch.name = "Sorcerer (Ghost)"
	sorcererAudio.play_sound("sorcerer_death")
	say(null, "But the sorcerer's magic wasn't what flared up. There was a low crackle and then the sorcerer's entire body was ablaze like a small bonfire. She was charred to a crisp before anyone had a chance to react. The ashes rose up to form a ghost.")
	step()
	sorcererAudio.play_sound("sorcerer_persuasion_fail_1")
	say("sorcerer", "I wonder, can you burn a ghost? Let's not find out. Anyway, I was answering your question...")
	step()
	jump("", "", "where_night_persuasion")
	
	end_event()

func where_night_persuasion():
	start_event("where_night_persuasion")
	
	step()
	sorcererAudio.play_sound("sorcerer_where_night_persuasion_0")
	say("sorcerer", "I didn’t see any people, but I did see some rats scurry out of the tavern. I think I might have heard a window opening, too, but I’m not sure where.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_night_insight():
	start_event("where_night_insight")
	
	step()
	sorcererAudio.play_sound("sorcerer_where_night_insight_0")
	say("sorcerer", "Alright, fine, there is a bit I left out. I didn’t want to look too suspicious, but I also don’t want anyone else to die from this curse, so I suppose I’ll have to tell you.")
	step()
	sorcererAudio.play_sound("sorcerer_where_night_insight_1")
	say("sorcerer", "When I went back to my room, I was fiddling around with the floorboards. I was bored and I figured better splinters than ashes. I accidentally discovered that it was rather easy to pull one loose silently. And then I realized I could reach through into the neighboring room, which of course was the cleric’s.")
	step()
	sorcererAudio.play_sound("sorcerer_where_night_insight_2")
	say("sorcerer", "Having done this much damage already, I decided I may as well pop my head through and take a look. The cleric had a little table next to her where she had some mundane jewelry: necklaces, bracelets, and the like.")
	step()
	sorcererAudio.play_sound("sorcerer_where_night_insight_3")
	say("sorcerer", "The amulet was not among them. And I can’t be certain, but it didn’t look like she was wearing it. Maybe it was already stolen at that point?")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_amulet():
	start_event("where_amulet")
	Tavern.change_image("Treasure")
	step()
	sorcererAudio.play_sound("sorcerer_where_amulet_0")
	say("sorcerer", "I was near the back, sandwiched between the druid and the bard. What a contrast of personalities, let me tell you.")
	step()
	sorcererAudio.play_sound("sorcerer_where_amulet_1")
	say("sorcerer", "The druid is the most nervous creature I’ve ever seen, and the bard has perhaps a little too much confidence for her own good. But, it all worked out fine, didn’t it? Well, maybe not so fine now...")
	step()
	
	jump("", "", "about_where")
	end_event()

func about_amulet():
	start_event("about_amulet")
	Tavern.change_image("Amulet")
	sorcererAudio.play_sound("sorcerer_amulet")
	say("sorcerer", "Oh yeah, that cool magic thing! What do you wanna know?")
	var choice = menu([
		["What do you know about the amulet?", "know", {}],
		["What would you do if you had the amulet?", "motive", {}],
		["What was it like when the amulet was discovered?", "discovered", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "know"):
		jump("", "", "amulet_know")
	elif cond(choice == "motive"):
		jump("", "", "amulet_motive")
	elif cond(choice == "discovered"):
		jump("", "", "amulet_discovered")
	end_event()

func amulet_know():
	start_event("amulet_know")
	
	step()
	sorcererAudio.play_sound("sorcerer_amulet_know_0")
	say("sorcerer", "It’s magic, it does protection, it’s cursed. I heard all the same things you did. I will say though, I’m curious how predictable the curse is.")
	step()
	sorcererAudio.play_sound("sorcerer_amulet_know_1")
	say("sorcerer", "Is it one of those things that’s literally random, is it based on the choices of some sort of sentient being, or does it follow some predetermined rules? I’m really not sure, but those are the questions I’d ask.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_menu():
	start_event("amulet_know_menu")
	
	step()
	if Settings.get("accessibility"):
		sorcererAudio.play_sounds(["1", "sorcerer_amulet_know_option_1", "2", "sorcerer_amulet_know_option_2"])
	say(null, """1. Do you have a lot of knowledge of these kinds of things?
2. Are you sure you’re telling me all you know?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "amulet_know_persuasion")
	elif cond(choice == "insight"):
		jump("", "", "amulet_know_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_know_persuasion():
	start_event("amulet_know_persuasion")
	
	step()
	sorcererAudio.play_sound("sorcerer_amulet_know_persuasion_0")
	say("sorcerer", "Nope! I’m just guessing. All I know of curses is what I’ve heard other people say about them.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_insight():
	start_event("amulet_know_insight")
	
	step()
	sorcererAudio.play_sound("sorcerer_amulet_know_insight_0")
	say("sorcerer", "Yes, I don’t know anything more. Ask someone smarter than me if you want to know.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_motive():
	start_event("amulet_motive")
	
	step()
	sorcererAudio.play_sound("sorcerer_amulet_motive_0")
	say("sorcerer", "As you may have noticed, I have a bit of a tendency to unintentionally cause chaos around me. While that can be fun, I wouldn’t say no to a bit more protection for my adventuring companions.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_motive_menu():
	start_event("amulet_motive_menu")
	
	step()
	if Settings.get("accessibility"):
		sorcererAudio.play_sounds(["1", "sorcerer_amulet_motive_option_1"])
	say(null, """1. Is that really what you would use the amulet for?""")
	var choice = menu([
		["1", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "insight"):
		jump("", "", "amulet_motive_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_motive_insight():
	start_event("amulet_motive_insight")
	
	step()
	sorcererAudio.play_sound("sorcerer_amulet_motive_insight_0")
	say("sorcerer", "Okay fine, you got me. If I had to choose only one person, it would probably be myself. Are you happy now?")
	step()
	sorcererAudio.play_sound("sorcerer_amulet_motive_insight_1")
	say("sorcerer", "It’s not a crime to be a little selfish. If the amulet did protect more than one person though, I definitely would have it protect my fellow adventurers.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()	

func amulet_discovered():
	start_event("amulet_discovered")
	
	step()
	sorcererAudio.play_sound("sorcerer_amulet_discovered_0")
	say("sorcerer", "Ah, that was a wild time! We were all wondering what that thing was and I don’t remember who started it but somehow we got to arguing about who should get it. Even though we already established the rules of treasure distribution. I guess that’s just what happens when you try to keep order in a large group.")
	step()
	
	jump("", "", "about_amulet")
	end_event()

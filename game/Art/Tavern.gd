extends Control

func change_image(img_name):
	for node in self.get_children():
		if node.name != img_name:
			if img_name == "Amulet" and node.name == "Tavern":
				node.show()
			elif node.visible:
				node.hide()
		else:
			node.show()

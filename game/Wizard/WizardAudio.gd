extends AudioStreamPlayer

const sounds = {
	"wizard_amulet_0": preload("res://assets/audio/va/wizard_amulet_0.wav"),
	"wizard_amulet_discovered_0": preload("res://assets/audio/va/wizard_amulet_discovered_0.wav"),
	"wizard_amulet_discovered_1": preload("res://assets/audio/va/wizard_amulet_discovered_1.wav"),
	"wizard_amulet_know_0": preload("res://assets/audio/va/wizard_amulet_know_0.wav"),
	"wizard_amulet_know_1": preload("res://assets/audio/va/wizard_amulet_know_1.wav"),
	"wizard_amulet_know_2": preload("res://assets/audio/va/wizard_amulet_know_2.wav"),
	"wizard_amulet_know_3": preload("res://assets/audio/va/wizard_amulet_know_3.wav"),
	"wizard_amulet_know_insight_0": preload("res://assets/audio/va/wizard_amulet_know_insight_0.wav"),
	"wizard_amulet_know_persuasion_0": preload("res://assets/audio/va/wizard_amulet_know_persuasion_0.wav"),
	"wizard_amulet_motive_0": preload("res://assets/audio/va/wizard_amulet_motive_0.wav"),
	"wizard_amulet_motive_1": preload("res://assets/audio/va/wizard_amulet_motive_1.wav"),
	"wizard_amulet_motive_2": preload("res://assets/audio/va/wizard_amulet_motive_2.wav"),
	"wizard_amulet_motive_insight_0": preload("res://assets/audio/va/wizard_amulet_motive_insight_0.wav"),
	"wizard_greeting": preload("res://assets/audio/va/wizard_greeting.wav"),
	"wizard_intro_0": preload("res://assets/audio/va/wizard_intro_0.wav"),
	"wizard_intro_1": preload("res://assets/audio/va/wizard_intro_1.wav"),
	"wizard_main": preload("res://assets/audio/va/wizard_main.wav"),
	"wizard_where": preload("res://assets/audio/va/wizard_where.wav"),
	"wizard_where_amulet_0": preload("res://assets/audio/va/wizard_where_amulet_0.wav"),
	"wizard_where_amulet_1": preload("res://assets/audio/va/wizard_where_amulet_1.wav"),
	"wizard_where_night_0": preload("res://assets/audio/va/wizard_where_night_0.wav"),
	"wizard_where_night_1": preload("res://assets/audio/va/wizard_where_night_1.wav"),
	"wizard_where_night_2": preload("res://assets/audio/va/wizard_where_night_2.wav"),
	"wizard_where_night_insight_0": preload("res://assets/audio/va/wizard_where_night_insight_0.wav"),
	"wizard_where_night_persuasion_0": preload("res://assets/audio/va/wizard_where_night_persuasion_0.wav"),
	"wizard_where_night_persuasion_1": preload("res://assets/audio/va/wizard_where_night_persuasion_1.wav"),
	"wizard_where_night_persuasion_2": preload("res://assets/audio/va/wizard_where_night_persuasion_2.wav"),
	"wizard_where_night_persuasion_3": preload("res://assets/audio/va/wizard_where_night_persuasion_3.wav"),
	"wizard_where_party_0": preload("res://assets/audio/va/wizard_where_party_0.wav"),
	"wizard_where_party_1": preload("res://assets/audio/va/wizard_where_party_1.wav"),
	"wizard_where_party_insight_0": preload("res://assets/audio/va/wizard_where_party_insight_0.wav"),
	"wizard_where_party_persuasion_0": preload("res://assets/audio/va/wizard_where_party_persuasion_0.wav"),
	"wizard_you": preload("res://assets/audio/va/wizard_you.wav"),
	"wizard_you_group_0": preload("res://assets/audio/va/wizard_you_group_0.wav"),
	"wizard_you_group_1": preload("res://assets/audio/va/wizard_you_group_1.wav"),
	"wizard_you_group_2": preload("res://assets/audio/va/wizard_you_group_2.wav"),
	"wizard_you_skills_0": preload("res://assets/audio/va/wizard_you_skills_0.wav"),
	"wizard_you_skills_1": preload("res://assets/audio/va/wizard_you_skills_1.wav"),
	"wizard_you_skills_2": preload("res://assets/audio/va/wizard_you_skills_2.wav"),
	"wizard_you_skills_3": preload("res://assets/audio/va/wizard_you_skills_3.wav"),
	"wizard_persuasion_fail_0": preload("res://assets/audio/va/wizard_persuasion_fail_0.wav"),
	"wizard_persuasion_fail_1": preload("res://assets/audio/va/wizard_persuasion_fail_1.wav"),
	"wizard_amulet_know_option_1": preload("res://assets/audio/va/accessibility/wizard_amulet_know_option_1.wav"),
	"wizard_amulet_know_option_2": preload("res://assets/audio/va/accessibility/wizard_amulet_know_option_2.wav"),
	"wizard_amulet_motive_option_1": preload("res://assets/audio/va/accessibility/wizard_amulet_motive_option_1.wav"),
	"wizard_death": preload("res://assets/audio/va/accessibility/wizard_death.wav"),
	"wizard_where_night_option_1": preload("res://assets/audio/va/accessibility/wizard_where_night_option_1.wav"),
	"wizard_where_night_option_2": preload("res://assets/audio/va/accessibility/wizard_where_night_option_2.wav"),
	"wizard_where_night_option_3": preload("res://assets/audio/va/accessibility/wizard_where_night_option_3.wav"),
	"wizard_where_party_option_1": preload("res://assets/audio/va/accessibility/wizard_where_party_option_1.wav"),
	"wizard_where_party_option_2": preload("res://assets/audio/va/accessibility/wizard_where_party_option_2.wav"),
	"wizard_where_party_option_3": preload("res://assets/audio/va/accessibility/wizard_where_party_option_3.wav"),
	"1": preload("res://assets/audio/va/accessibility/1.wav"),
	"2": preload("res://assets/audio/va/accessibility/2.wav"),
	"3": preload("res://assets/audio/va/accessibility/3.wav"),
}

func play_sound(sound_name):
	print(self.name, " playing sound: ", sound_name)
	if !self.sounds.has(sound_name):
		print(self.name, " skipping sound: ", sound_name)
		return
	self.stream = self.sounds[sound_name]
	self.play()

func play_sounds(sounds):
	for sound in sounds:
		self.play_sound(sound)
		yield(self, "finished")

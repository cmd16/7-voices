extends Dialogue

onready var ch = Rakugo.define_character("Wizard" if !Rakugo.store.get("wizard_dead") else "Wizard (Ghost)", "wizard", Color("#88CCEE"))
onready var wizardAudio = get_node("../WizardAudio")

"""
About you
	What are you good at?
	Why are you with this group?
About your whereabouts
	Where were you during the party?
	Where were you last night?
	Where were you when the amulet was discovered?
About the amulet
	What do you know about the amulet?
	What would you do if you had the amulet?
	What was it like when the amulet was discovered?
"""

func first_dialogue():
	start_event("first_dialogue")
	Tavern.change_image("Tavern")
	wizardAudio.play_sound("wizard_greeting")
	say("wizard", "Greetings! Thank you for agreeing to help us. I'll be happy to share any information that may be useful.")
	
	step()
	wizardAudio.play_sound("wizard_main")
	say("wizard", "What would you like to talk about?")
	var start_topic = menu([
		["About you", "you", {}],
		["About your whereabouts", "where", {}],
		["About the amulet", "amulet", {}],
		["No more questions.", "done", {}]
	])
	step()
	
	say(null, "Choose a question")
	if cond(start_topic == "you"):
		jump("", "", "about_you")
	elif cond(start_topic == "where"):
		jump("", "", "about_where")
	elif cond(start_topic == "amulet"):
		jump("", "", "about_amulet")
	else:
		Music.play_tavern()
		jump("CharacterChoose", "", "select_character")
	
	end_event()

func about_you():
	start_event("about_you")
	Tavern.change_image("Question")
	wizardAudio.play_sound("wizard_you")
	say("wizard", "What do you want to know about me?")
	var choice = menu([
			["What are you good at?", "skills", {}],
			["Why are you with this group?", "group", {}],
			["Never mind.", "back", {}]
		])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "skills"):
		jump("", "", "you_skills")
	elif cond(choice == "group"):
		jump("", "", "you_group")
	end_event()

func you_skills():
	start_event("you_skills")
	
	step()
	wizardAudio.play_sound("wizard_you_skills_0")
	say("wizard", "As you may know, I have been considered the brains of the group. I’m the one who has a great background of knowledge, and I’m the one who does research.")
	step()
	wizardAudio.play_sound("wizard_you_skills_1")
	say("wizard", "I’ve studied a variety of schools of magic, but I suppose I’m particular to transmutation: changing one kind of matter into something else.")
	step()
	wizardAudio.play_sound("wizard_you_skills_2")
	say("wizard", "The rogue has been requesting that I turn things into gold, but I’m not quite at that level yet, and besides, I’m concerned about the long-term effects on the economy. Dungeon delving complicates things enough.")
	step()
	wizardAudio.play_sound("wizard_you_skills_3")
	say("wizard", "In fact, with the increase of adventuring groups lately, the magical shops have had a surplus of rare items, which of course makes the items less rare and then the poor shopkeepers will go out of business! We have to find a solution to this! I haven’t found one yet, but I’ll keep looking.")
	step()
	
	jump("", "", "about_you")
	end_event()

func you_group():
	start_event("you_group")

	step()
	wizardAudio.play_sound("wizard_you_group_0")
	say("wizard", "I realized that poring over tomes in the library wasn’t getting me quite as much information as I’d like.")
	step()
	wizardAudio.play_sound("wizard_you_group_1")
	say("wizard", "So, in my quest for knowledge, I decided to take a risk and join an adventuring party that I heard was looking for a scholar.")
	step()
	wizardAudio.play_sound("wizard_you_group_2")
	say("wizard", "And I’m so glad I did! I’ve found all sorts of amazing things in my time with them. Did you know, there are books that will try to eat your face off? The biology of it is fascinating!")
	step()
	
	jump("", "", "about_you")
	end_event()

func about_where():
	start_event("about_where")
	Tavern.change_image("Tavern")
	wizardAudio.play_sound("wizard_where")
	say("wizard", "What time are you asking about?")
	var choice = menu([
		["Where were you when the amulet was discovered?", "amulet", {}],
		["Where were you during the party?", "party", {}],
		["Where were you after the party?", "night", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "party"):
		jump("", "", "where_party")
	elif cond(choice == "night"):
		jump("", "", "where_night")
	elif cond(choice == "amulet"):
		jump("", "", "where_amulet")
	
	end_event()

func where_party():
	start_event("where_party")
	Tavern.change_image("Party")
	step()
	wizardAudio.play_sound("wizard_where_party_0")
	say("wizard", "I was off in that corner table over on the right, studying the traces I’d made of the amulet.")
	step()
	wizardAudio.play_sound("wizard_where_party_1")
	say("wizard", "The whole process is really quite fascinating! If you had told me a year ago that I’d be able to learn so much about a magical item based on an initial scan, I would have laughed you out of the library!")
	step()
	
	jump("", "", "where_party_menu")
	
	end_event()

func where_party_menu():
	start_event("where_party_menu")
	
	step()
	wizardAudio.play_sounds(["1", "wizard_where_party_option_1", "2", "wizard_where_party_option_2", "3", "wizard_where_party_option_3"])
	say(null, """1. Did you notice anyone else, or were you too engrossed in that fascinating work?
2. So what you're telling me is, you were too focused on your studies to notice anyone else?
3. Are you sure you haven’t forgotten something?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "persuasion_fail", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_party_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("wizard_dead"):
			jump("", "", "where_party_persuasion")
		else:
			Rakugo.store.set("wizard_dead", true)
			jump("", "", "where_party_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_party_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_party_persuasion_fail():
	start_event("where_party_persuasion_fail")
	
	step()
	wizardAudio.play_sound("wizard_persuasion_fail_0")
	say("wizard", "I'm getting a little impatient here, so I suppose now is a good time to test my theory about the curse. Thank you for all your hard work. I'm sure you'll solve this. Ahem! I, uh... I mean to do you bodily harm!")
	step()
	ch.name = "Wizard (Ghost)"
	wizardAudio.play_sound("wizard_death")
	say(null, "A book rose out of the wizards bag and proceeded to eat her face off. She had sacrificed herself to prove her theory! Was it worth it? I don't know, but I do know that she rose as a ghost afterwards.")
	step()
	wizardAudio.play_sound("wizard_persuasion_fail_1")
	say("wizard", "I'm a ghost now. Fascinating! This opens up so many more opportunities for study. Of course, I'm sure some things are harder while incorporeal. Anyway, back to your question.")
	step()
	jump("", "", "where_party_persuasion")
	
	end_event()

func where_party_persuasion():
	start_event("where_party_persuasion")
	
	step()
	wizardAudio.play_sound("wizard_where_party_persuasion_0")
	say("wizard", "Now that I think about it, there was a moment where I thought I saw someone peering over my shoulder. It passed rather quickly, and I never figured out who it was.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_party_insight():
	start_event("where_party_insight")
	
	step()
	wizardAudio.play_sound("wizard_where_party_insight_0")
	say("wizard", "Oh, I think there was a moment where there was some kind of fire or something? I ignored it, but I suppose it may have distracted other people.")
	step()
	
	jump("", "", "where_party_menu")
	end_event()

func where_night():
	start_event("where_night")
	Tavern.change_image("Bedroom")
	step()
	wizardAudio.play_sound("wizard_where_night_0")
	say("wizard", "Sleeping? I’m not in college anymore, I don’t do all-nighters.")
	step()
	wizardAudio.play_sound("wizard_where_night_1")
	say("wizard", "Oh, right, where I was sleeping. I spent the night with the bard, as usual. We were in the third room on the right. It actually took me a while to fall asleep, because I was delightfully intrigued by the floorboards.")
	step()
	wizardAudio.play_sound("wizard_where_night_2")
	say("wizard", "They seem to be composed of a combination of multiple kinds of wood, but I can’t figure out why anyone would want to do that. I wonder, is the wood more pliable? Could someone sneak through the floorboards?")
	step()
	
	jump("", "", "where_night_menu")
	
	end_event()

func where_night_menu():
	start_event("where_night_menu")
	
	step()
	wizardAudio.play_sounds(["1", "wizard_where_night_option_1", "2", "wizard_where_night_option_2", "3", "wizard_where_night_option_3"])
	say(null, """1. What a fascinating theory! Would you care to explain a little more?
2. I'm not convinced. Could you explain further?
3. That story is oddly specific. How do I know you’re not making it up?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "persuasion_fail"],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	
	if cond(choice == "persuasion"):
		jump("", "", "where_night_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("wizard_dead"):
			jump("", "", "where_night_persuasion")
		else:
			Rakugo.store.set("wizard_dead", true)
			jump("", "", "where_night_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_night_insight")
	else:
		jump("", "", "about_where")
	
	end_event()

func where_night_persuasion_fail():
	start_event("where_night_persuasion_fail")
	
	step()
	wizardAudio.play_sound("wizard_persuasion_fail_0")
	say("wizard", "I'm getting a little impatient here, so I suppose now is a good time to test my theory about the curse. Thank you for all your hard work. I'm sure you'll solve this. Ahem! I, uh... I mean to do you bodily harm!")
	step()
	ch.name = "Wizard (Ghost)"
	wizardAudio.play_sound("wizard_death")
	say(null, "A book rose out of the wizards bag and proceeded to eat her face off. She had sacrificed herself to prove her theory! Was it worth it? I don't know, but I do know that she rose as a ghost afterwards.")
	step()
	wizardAudio.play_sound("wizard_persuasion_fail_1")
	say("wizard", "I'm a ghost now. Fascinating! This opens up so many more opportunities for study. Of course, I'm sure some things are harder to while incorporeal. Anyway, back to your question.")
	step()
	jump("", "", "where_night_persuasion")
	
	end_event()

func where_night_persuasion():
	start_event("where_night_persuasion")
	
	step()
	wizardAudio.play_sound("wizard_where_night_persuasion_0")
	say("wizard", "Oh, you want to hear more about my study? Certainly.")
	step()
	wizardAudio.play_sound("wizard_where_night_persuasion_1")
	say("wizard", "First of all, this wood seems to be very quiet. No creaky floorboards, so I’m afraid that’s one means of evidence unavailable to you.")
	step()
	wizardAudio.play_sound("wizard_where_night_persuasion_2")
	say("wizard", "Secondly, I think these floorboards could be removed to allow someone to climb from room to room.")
	step()
	wizardAudio.play_sound("wizard_where_night_persuasion_3")
	say("wizard", "I mean, they’d have to be good at moving around like that, but I think some people in our group could manage it. Though if the wall is like that too, then there would be even less skill required.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_night_insight():
	start_event("where_night_insight")
	
	step()
	wizardAudio.play_sound("wizard_where_night_insight_0")
	say("wizard", "Is it that hard to imagine that I would get excited by architecture? I promise, I’m telling the truth.")
	step()
	
	jump("", "", "where_night_menu")
	end_event()

func where_amulet():
	start_event("where_amulet")
	Tavern.change_image("Treasure")
	step()
	wizardAudio.play_sound("wizard_where_amulet_0")
	say("wizard", "I was near the middle of the group. I’m a little bit fragile, so I try to keep out of the way of any potential attacks.")
	step()
	wizardAudio.play_sound("wizard_where_amulet_1")
	say("wizard", "I didn’t see the amulet myself, I just heard that gasp of delight that usually indicates powerful treasure, and then I heard the sound of said treasure being put into a bag.")
	step()
	
	jump("", "", "about_where")
	end_event()

func about_amulet():
	start_event("about_amulet")
	Tavern.change_image("Amulet")
	wizardAudio.play_sound("wizard_amulet_0")
	say("wizard", "What do you want to know about the amulet?")
	var choice = menu([
		["What do you know about the amulet?", "know", {}],
		["What would you do if you had the amulet?", "motive", {}],
		["What was it like when the amulet was discovered?", "discovered", {}],
		["Never mind.", "back", {}]
	])
	step()
	
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "know"):
		jump("", "", "amulet_know")
	elif cond(choice == "motive"):
		jump("", "", "amulet_motive")
	elif cond(choice == "discovered"):
		jump("", "", "amulet_discovered")
	end_event()

func amulet_know():
	start_event("amulet_know")
	
	step()
	wizardAudio.play_sound("wizard_amulet_know_0")
	say("wizard", "As I said earlier, the amulet is cursed. I’ve been doing some more study with the information I have and comparing it to other cursed artifacts that have been found, and I think I know a little more about the nature of the curse.")
	step()
	wizardAudio.play_sound("wizard_amulet_know_1")
	say("wizard", "You see, there seem to be other magical artifacts made by the same group, and they share a certain curse. The item is able to sense the situation around it, and it can tell when the group in control of it is in a... less than harmonious state.")
	step()
	wizardAudio.play_sound("wizard_amulet_know_2")
	say("wizard", "The item desires unity and abhors infighting, so if it detects such a detestable situation, it will murder one of the participants.")
	step()
	wizardAudio.play_sound("wizard_amulet_know_3")
	say("wizard", "I haven’t told anyone else. I’m afraid of causing a panic. So please, just keep this between the two of us.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_menu():
	start_event("amulet_know_menu")
	
	step()
	wizardAudio.play_sounds(["1", "wizard_amulet_know_option_1", "2", "wizard_amulet_know_option_2"])
	say(null, """1. That sounds concerning. Do you know anything else? I’m just trying to keep us safe.
2. Seems like a very convenient curse, if that inspires complacency. Are you sure that’s how it works?
""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "insight", {}],
		["Done", "done", {}]
	])
	step()
	
	if cond(choice == "persuasion"):
		jump("", "", "amulet_know_persuasion")
	elif cond(choice == "insight"):
		jump("", "", "amulet_know_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_know_persuasion():
	start_event("amulet_know_persuasion")
	
	step()
	wizardAudio.play_sound("wizard_amulet_know_persuasion_0")
	say("wizard", "As far as I can tell, the person the amulet kills is the first one to respond with aggression. So if you say something upsetting and I get mad, I’m the one it would kill.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_know_insight():
	start_event("amulet_know_insight")
	
	step()
	wizardAudio.play_sound("wizard_amulet_know_insight_0")
	say("wizard", "No, I’m not sure, but it’s the best guess I have so far, and I’m usually pretty good at these things. You’re right that it’s very convenient though. Almost like it was perfectly suited to our group... but I’m sure that’s just a coincidence.")
	step()
	
	jump("", "", "amulet_know_menu")
	
	end_event()

func amulet_motive():
	start_event("amulet_motive")
	
	step()
	wizardAudio.play_sound("wizard_amulet_motive_0")
	say("wizard", "I would be able to do even better studies than I am doing currently. Traces are useful, but a physical copy allows more in-depth work.")
	step()
	wizardAudio.play_sound("wizard_amulet_motive_1")
	say("wizard", "Perhaps I could even go so far as to make a copy! That would be wonderful! Imagine the potential for peace and protection.")
	step()
	wizardAudio.play_sound("wizard_amulet_motive_2")
	say("wizard", "Though that does introduce a potential problem of a ruling elite holding onto copies of the amulet... I’d need to give some serious thought to the consequences of distribution before I went too far with the practical part of my studies.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_motive_menu():
	start_event("amulet_motive_menu")
	
	step()
	wizardAudio.play_sounds(["1", "wizard_amulet_motive_option_1"])
	say(null, """1. Is that really your whole plan? No extra steps or twists?""")
	var choice = menu([
		["1", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "insight"):
		jump("", "", "amulet_motive_insight")
	else:
		jump("", "", "about_amulet")
	
	end_event()

func amulet_motive_insight():
	start_event("amulet_motive_insight")
	
	step()
	wizardAudio.play_sound("wizard_amulet_motive_insight_0")
	say("wizard", "Yes, that’s the whole plan.")
	step()
	
	jump("", "", "amulet_motive_menu")
	
	end_event()

func amulet_discovered():
	start_event("amulet_discovered")
	
	step()
	wizardAudio.play_sound("wizard_amulet_discovered_0")
	say("wizard", "I feel like it all happened so quickly. It’s like the item was put away almost immediately after discovery. Perhaps the cleric anticipated the potential for infighting and wanted to make sure we didn’t have any arguments while we were still in the danger of the dungeon?")
	step()
	wizardAudio.play_sound("wizard_amulet_discovered_1")
	say("wizard", "There was still a bit of arguing, but we brought most of the arguments to the tavern instead, and look where that got us.")
	step()
	
	jump("", "", "about_amulet")
	end_event()

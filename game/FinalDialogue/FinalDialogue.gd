extends Dialogue

onready var bard_ch = Rakugo.define_character("Bard" if !Rakugo.store.get("bard_dead") else "Bard (Ghost)", "bard", Color("#44AA99"))
onready var cleric_ch = Rakugo.define_character("Cleric" if !Rakugo.store.get("cleric_dead") else "Cleric (Ghost)", "cleric", Color("#AA4499"))
onready var druid_ch = Rakugo.define_character("Druid" if !Rakugo.store.get("druid_dead") else "Druid (Ghost)", "druid", Color("#117733"))
onready var paladin_ch = Rakugo.define_character("Paladin" if !Rakugo.store.get("paladin_dead") else "Paladin (Ghost)", "paladin", Color("#DDCC77"))
onready var rogue_ch = Rakugo.define_character("Rogue" if !Rakugo.store.get("rogue_dead") else "Rogue (Ghost)", "rogue", Color("#332288"))
onready var sorcerer_ch = Rakugo.define_character("Sorcerer" if !Rakugo.store.get("sorcerer_dead") else "Sorcerer (Ghost)", "sorcerer", Color("#882255"))
onready var wizard_ch = Rakugo.define_character("Wizard" if !Rakugo.store.get("wizard_dead") else "Wizard (Ghost)", "wizard", Color("#88CCEE"))

onready var audioPlayer = get_parent().get_node("AudioPlayer")

func first_dialogue():
	start_event("first_dialogue")
	
	say(null, "It's time to tell everyone my conclusion: who stole the amulet?")
	var suspect = menu([
		["Bard", "bard", {}],
		["Cleric", "cleric", {}],
		["Druid", "druid", {}],
		["Paladin", "paladin", {}],
		["Rogue", "rogue", {}],
		["Sorcerer", "sorcerer", {}],
		["Wizard", "wizard", {}]
	])
	step()
	
	if cond(suspect == "rogue"):
		audioPlayer.play_sound("rogue_final_0")
		say("rogue", "I stole the amulet. But it was just a quick pickpocket towards the end of the party to make sure my skills were still sharp. I put it back right afterwards. So I wasn't the only thief.")
	elif cond(suspect == "druid"):
		audioPlayer.play_sound("druid_final_0")
		say("druid", "I'm sorry! I just really wanted to help my sister. Last night I turned into a rat, snuck into the cleric's room, and ran off with the amulet. I waited to leave the cleric's room until the rogue left, so she didn't see me.")
	elif cond(suspect == "sorcerer"):
		audioPlayer.play_sound("sorcerer_final_0")
		say("sorcerer", "I would have stolen the amulet, but I couldn't find it! I went through the floorboards into the cleric's room intending to steal the amulet, but when I got there it was already gone.")
	elif cond(suspect == "cleric"):
		audioPlayer.play_sound("cleric_final_0")
		say("cleric", "How would I steal the amulet I already had? But I have to give you credit for suspecting me. I did know more than I let on. I'll explain, but first, who stole the amulet?")
	elif cond(suspect == "bard"):
		audioPlayer.play_sound("bard_final_0")
		say("bard", "Not at all. Must have been someone else.")
	elif cond(suspect == "wizard"):
		audioPlayer.play_sound("wizard_final_0")
		say("wizard", "No, I was sure I'd get a chance to study the amulet in due course.")
	elif cond(suspect == "paladin"):
		audioPlayer.play_sound("paladin_final_0")
		say("paladin", "You continue to disappoint me. Now, will the real culprit please reveal themselves?")
	step()
	if cond(suspect != "sorcerer"):
		audioPlayer.play_sound("sorcerer_final_0")
		say("sorcerer", "I would have stolen the amulet, but I couldn't find it! I went through the floorboards into the cleric's room intending to steal the amulet, but when I got there it was already gone.")
		step()
	if cond(suspect != "rogue"):
		audioPlayer.play_sound("rogue_final_0")
		say("rogue", "I stole the amulet. But it was just a quick pickpocket towards the end of the party to make sure my skills were still sharp. I put it back right afterwards. So I wasn't the only thief.")
		step()
	if cond(suspect != "druid"):
		audioPlayer.play_sound("druid_final_0")
		say("druid", "I'm sorry! I just really wanted to help my sister. Last night I turned into a rat, snuck into the cleric's room, and ran off with the amulet. I waited to leave the cleric's room until the rogue left, so she didn't see me.")
		step()
	
	var death_count = 0
	if Rakugo.store.get("bard_dead"):
		death_count += 1
	if Rakugo.store.get("sleric_dead"):
		death_count += 1
	if Rakugo.store.get("druid_dead"):
		death_count += 1
	if Rakugo.store.get("paladin_dead"):
		death_count += 1
	if Rakugo.store.get("rogue_dead"):
		death_count += 1
	if Rakugo.store.get("sorcerer_dead"):
		death_count += 1
	if Rakugo.store.get("wizard_dead"):
		death_count += 1
	
	audioPlayer.play_sound("cleric_final_1")
	say("cleric", "I think it's time I come clean. I'm sorry for dragging everyone into this mess.")
	step()
	if death_count > 0:
		audioPlayer.play_sound("cleric_final_2")
		say("cleric", "Especially those of you who died as a result of the curse.")
		step()
	audioPlayer.play_sound("cleric_final_3")
	say("cleric", "I recognized the amulet immediately when I saw it. A certain religious cult has made these various artifacts as a sort of test of character. They carry both a powerful benefit and a deadly curse.")
	step()
	audioPlayer.play_sound("cleric_final_4")
	say("cleric", "The power varies, but the curse is the same: it is triggered by a hostile act within a group (such as theft) and it murders anyone who becomes hostile to the group.")
	step()
	audioPlayer.play_sound("cleric_final_5")
	say("cleric", "I was going to try to keep it a secret. But frankly, I was tired of people in our group causing trouble. So I decided to make it a little easy to steal, and I figured this would be a good way to weed out the unscrupulous members of our group.")
	step()
	if death_count == 0:
		audioPlayer.play_sound("cleric_final_6")
		say("cleric", "Miraculously, we've made it out with no casualties.")
		step()
	elif death_count < 4:
		audioPlayer.play_sound("cleric_final_7")
		say("cleric", "It seemed that the amulet did in fact weed out a few people.")
		step()
	elif death_count < 7:
		audioPlayer.play_sound("cleric_final_8")
		say("cleric", "I can't say I was expecting this level of carnage. But maybe we deserved it.")
		step()
	else:
		audioPlayer.play_sound("cleric_final_9")
		say("cleric", "And this ended with a complete ghost crew. I suppose if you want to keep adventuring as ghosts, I think that's possible.")
		step()
	audioPlayer.play_sound("cleric_final_10")
	say("cleric", "Regardless, I will be stepping down from my position as leader. And from my position as a follower of the god of protection. I suppose I'll have to find a new deity now.")
	step()
	audioPlayer.play_sound("paladin_final_1")
	say("paladin", "Well, of all the possible outcomes, that was... not what I was expecting.")
	step()
	audioPlayer.play_sound("paladin_final_2")
	say("paladin", "I suppose I'm the most fit to lead, given the cleric's disgrace. But I'm not sure if it's worth it, given this group's track record. What do you think?")
	step()
	
	if Settings.get("accessibility"):
		audioPlayer.play_sound("stay_question")
	say(null, "Should the paladin stay with the group or not?")
	var choice = menu([
		["Stay with the group", "stay", {}],
		["Leave the group", "leave", {}]
	])
	step()
	
	if cond(choice == "stay"):
		audioPlayer.play_sound("stay_speech")
		say(null, "I think you should give them another chance. Maybe they can improve under your leadership.")
		step()
		audioPlayer.play_sound("paladin_final_3")
		say("paladin", "Very well then, I will try. Thank you for your help.")
		step()
		audioPlayer.play_sound("stay_result")
		say(null, "Well, the group is together for now at least. Who knows how long that will last. Anyway, there's another mystery solved.")
		step()
	else:
		audioPlayer.play_sound("leave_speech")
		say(null, "I think you and I both know you're just looking for permission to move on. Go ahead, then. You can do better.")
		step()
		audioPlayer.play_sound("paladin_final_4")
		say("paladin", "I suppose you're right. Thank you. Goodbye, adventurers, and good riddance!")
		step()
		audioPlayer.play_sound("leave_result")
		say(null, "There was an awkward silence as nobody else tried to claim leadership. The silence persisted. Eventually, everyone went off their separate ways. Seems like that's another group fractured by infighting. But were they ever that strong to begin with? Anyway, my work here is done.")
		step()
	
	audioPlayer.play_sound("thank_you")
	say(null, "Thank you for playing 7 Voices!")
	
	end_event()

extends AudioStreamPlayer

const sounds = {
	"bard_final_0": preload("res://assets/audio/va/bard_final_0.wav"),
	"cleric_final_0": preload("res://assets/audio/va/cleric_final_0.wav"),
	"cleric_final_1": preload("res://assets/audio/va/cleric_final_1.wav"),
	"cleric_final_2": preload("res://assets/audio/va/cleric_final_2.wav"),
	"cleric_final_3": preload("res://assets/audio/va/cleric_final_3.wav"),
	"cleric_final_4": preload("res://assets/audio/va/cleric_final_4.wav"),
	"cleric_final_5": preload("res://assets/audio/va/cleric_final_5.wav"),
	"cleric_final_6": preload("res://assets/audio/va/cleric_final_6.wav"),
	"cleric_final_7": preload("res://assets/audio/va/cleric_final_7.wav"),
	"cleric_final_8": preload("res://assets/audio/va/cleric_final_8.wav"),
	"cleric_final_9": preload("res://assets/audio/va/cleric_final_9.wav"),
	"cleric_final_10": preload("res://assets/audio/va/cleric_final_10.wav"),
	"druid_final_0": preload("res://assets/audio/va/druid_final_0.wav"),
	"paladin_final_0": preload("res://assets/audio/va/paladin_final_0.wav"),
	"paladin_final_1": preload("res://assets/audio/va/paladin_final_1.wav"),
	"paladin_final_2": preload("res://assets/audio/va/paladin_final_2.wav"),
	"paladin_final_3": preload("res://assets/audio/va/paladin_final_3.wav"),
	"paladin_final_4": ("res://assets/audio/va/paladin_final_4.wav"),
	"rogue_final_0": preload("res://assets/audio/va/rogue_final_0.wav"),
	"sorcerer_final_0": preload("res://assets/audio/va/sorcerer_final_0.wav"),
	"wizard_final_0": preload("res://assets/audio/va/wizard_final_0.wav"),
	"thank_you": preload("res://assets/audio/va/thank_you.wav"),
	"stay_speech": preload("res://assets/audio/va/accessibility/stay_speech.wav"),
	"stay_result": preload("res://assets/audio/va/accessibility/stay_result.wav"),
	"leave_speech": preload("res://assets/audio/va/accessibility/leave_speech.wav"),
	"leave_result": preload("res://assets/audio/va/accessibility/leave_result.wav"),
	"stay_question": preload("res://assets/audio/va/accessibility/stay_question.wav")
}

func play_sound(sound_name):
	print(self.name, " playing sound: ", sound_name)
	if !self.sounds.has(sound_name):
		print(self.name, " skipping sound: ", sound_name)
		return
	self.stream = self.sounds[sound_name]
	self.play()

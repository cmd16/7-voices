extends Dialogue

onready var ch = Rakugo.define_character("Bard" if !Rakugo.store.get("bard_dead") else "Bard (Ghost)", "bard", Color("#44AA99"))
onready var audioPlayer = get_parent().get_node("BardAudio")

"""
About you
	What are you good at?
	Why are you with this group?
About your whereabouts
	Where were you during the party?
	Where were you last night?
	Where were you when the amulet was discovered?
About the amulet
	What do you know about the amulet?
	What would you do if you had the amulet?
	What was it like when the amulet was discovered?
"""

func first_dialogue():
	start_event("first_dialogue")
	Tavern.change_image("Tavern")
	audioPlayer.play_sound("bard_greeting")
	say("bard", "Well hello, darling!")
	step()
	audioPlayer.play_sound("bard_main")
	say("bard", "How may I help you?")
	var start_topic = menu([
		["About you", "you", {}],
		["About your whereabouts", "where", {}],
		["About the amulet", "amulet", {}],
		["No more questions.", "done", {}]
	])
	step()
	if cond(start_topic == "you"):
		jump("", "", "about_you")
	elif cond(start_topic == "where"):
		jump("", "", "about_where")
	elif cond(start_topic == "amulet"):
		jump("", "", "about_amulet")
	else:
		Music.play_tavern()
		jump("CharacterChoose", "", "select_character")
	end_event()

func about_you():
	start_event("about_you")
	Tavern.change_image("Question")
	audioPlayer.play_sound("bard_you")
	say("bard", "I'm an open book, honey.")
	var choice = menu([
			["What are you good at?", "skills", {}],
			["Why are you with this group?", "group", {}],
			["Never mind.", "back", {}]
		])
	step()
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "skills"):
		jump("", "", "you_skills")
	elif cond(choice == "group"):
		jump("", "", "you_group")
	end_event()

func you_skills():
	start_event("you_skills")
	step()
	audioPlayer.play_sound("bard_you_skills_0")
	say("bard", "My dear, have you never met a bard before? Surely you’ve heard about us excellent musicians. Here, I’ll give you a little something now.")
	step()
	audioPlayer.play_sound("bard_you_skills_1")
	say("bard", "Hey, y’all I’m a bard, and I sing with my heart. I’m ready to help my friends out with healing, bonuses, or charms!")
	step()
	audioPlayer.play_sound("bard_you_skills_2")
	say("bard", "I can be mighty persuasive when called for, or, if you prefer, I’m perfectly happy to hurl down insults because let’s face it, there’s a lot of disappointing people out there in the world.")
	step()
	jump("", "", "about_you")
	end_event()

func you_group():
	start_event("you_group")
	step()
	audioPlayer.play_sound("bard_you_group_0")
	say("bard", "I go wherever my fancy takes me, and right now I love my wonderful crew! They’re always happy to listen to a story or two. And there’s plenty of people for me to support!")
	step()
	audioPlayer.play_sound("bard_you_group_1")
	say("bard", "And just between you and me, that wizard over there, you know the nerdy one? She’s mighty cute. There is something very attractive about getting that excited.")
	step()
	jump("", "", "about_you")
	end_event()

func about_where():
	start_event("about_where")
	Tavern.change_image("Tavern")
	audioPlayer.play_sound("bard_where")
	say("bard", "I've been around quite a bit, sugar. You're gonna have to be a little more specific.")
	var choice = menu([
		["Where were you when the amulet was discovered?", "amulet", {}],
		["Where were you during the party?", "party", {}],
		["Where were you after the party?", "night", {}],
		["Never mind.", "back", {}]
	])
	step()
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "party"):
		jump("", "", "where_party")
	elif cond(choice == "night"):
		jump("", "", "where_night")
	elif cond(choice == "amulet"):
		jump("", "", "where_amulet")
	end_event()

func where_party():
	start_event("where_party")
	Tavern.change_image("Party")
	step()
	audioPlayer.play_sound("bard_where_party_0")
	say("bard", "Well I’m always down for a round of singing. I tried to get the druid in on it this time, but she was not having it. That’s alright though. I’m happy to sing even if I’m the only one doing it.")
	step()
	audioPlayer.play_sound("bard_where_party_1")
	say("bard", "The sorcerer did join me for a bit, she’s always down for a little fun. She had to leave when she accidentally blew up the table though. That was quite the commotion!")
	step()
	audioPlayer.play_sound("bard_where_party_2")
	say("bard", "Oh, and I got the cleric in for a little bit of singing as well. She likes to pretend she’s all prim and proper, but she’s happy to let loose like the rest of us so long as it doesn’t impede her judgement too much.")
	step()
	jump("", "", "where_party_menu")
	end_event()

func where_party_menu():
	start_event("where_party_menu")
	step()
	if Settings.get("accessibility"):
		audioPlayer.play_sounds(["1", "bard_where_party_option_1", "2", "bard_where_party_option_2", "3", "bard_where_party_option_3"])
	say(null, """1. I'm surprised you got anyone to come over, given your singing skills! Could you give me a timeline of people’s whereabouts based on what you saw?
2. Sounds like you were pretty popular: you saw almost everybody! Could you give me a timeline of people’s whereabouts based on what you saw?
3. Are you sure that's all you did?""")
	var choice = menu([
		["1", "persuasion_fail", {}],
		["2", "persuasion", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "persuasion"):
		jump("", "", "where_party_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("bard_dead"):
			jump("", "", "where_party_persuasion")
		else:
			Rakugo.store.set("bard_dead", true)
			jump("", "", "where_party_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_party_insight")
	else:
		jump("", "", "about_where")
	end_event()

func where_party_persuasion_fail():
	start_event("where_party_persuasion_fail")
	step()
	audioPlayer.play_sound("bard_persuasion_fail_0")
	say("bard", "Well, there's no need to be rude about it! Your constant questioning is really getting on my nerves. I ought to show you what that feels like...")
	step()
	ch.name = "Bard (Ghost)"
	audioPlayer.play_sound("bard_death")
	say(null, "Before the bard could demonstrate what she meant by that, she was drowned out by a loud power chord coming from her lute. It rose up and clubbed her in the head. She fell to the ground, and a quick check of her vitals revealed she was not just unconscious. A few moments later, she rose as a ghost.")
	step()
	audioPlayer.play_sound("bard_persuasion_fail_1")
	say("bard", "I suppose I may as well answer your questions.")
	step()
	jump("", "", "where_party_persuasion")
	end_event()

func where_party_persuasion():
	start_event("where_party_persuasion")
	step()
	audioPlayer.play_sound("bard_where_party_persuasion_0")
	say("bard", "Sure. I’ll start with the people I didn’t interact with: the rogue was off in one corner and the wizard in the other. I didn’t do anything with the paladin; I know she’s too pompous for something like music. She was in the center of the room I think, with the cleric.")
	step()
	audioPlayer.play_sound("bard_where_party_persuasion_1")
	say("bard", "So, the first person I tried to recruit was the druid. She said no right away and ran off.")
	step()
	audioPlayer.play_sound("bard_where_party_persuasion_2")
	say("bard", "Shortly afterwards I got the sorcerer to come over. She left after fixing up the fire. Then there was a while where I was just singing lonely by myself.")
	step()
	audioPlayer.play_sound("bard_where_party_persuasion_3")
	say("bard", "Eventually, the cleric wandered over and we had a great time. She went to the paladin after that I think. And then it was bedtime.")
	step()
	jump("", "", "where_party_menu")
	end_event()

func where_party_insight():
	start_event("where_party_insight")
	step()
	audioPlayer.play_sound("bard_where_party_insight_0")
	say("bard", "Yep, singing keeps me pretty busy.")
	step()
	jump("", "", "where_party_menu")
	end_event()

func where_night():
	start_event("where_night")
	Tavern.change_image("Bedroom")
	step()
	audioPlayer.play_sound("bard_where_night_0")
	say("bard", "Oh, I was in the third room on the right with the wizard. She kept going on about the floorboards, something about the wood being interesting? I don’t really get it. But she was having fun, so I just kinda smiled at her.")
	step()
	audioPlayer.play_sound("bard_where_night_1")
	say("bard", "Then I worried I’d been staring a little too long, so I pulled out my lute to try and figure out some more of that tune I’d been working on. Eventually, we went to bed.")
	step()
	jump("", "", "where_night_menu")
	end_event()

func where_night_menu():
	start_event("where_night_menu")
	step()
	if Settings.get("accessibility"):
		audioPlayer.play_sounds(["1", "bard_where_night_option_1", "2", "bard_where_night_option_2", "3", "bard_where_night_option_3"])
	say(null, """1. Sounds like fun! Did you notice anything else interesting?
2. Stick to the relevant information, please. Did you notice anything else interesting?
3. Are you sure you stayed there all night?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "persuasion_fail", {}],
		["3", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "persuasion"):
		jump("", "", "where_night_persuasion")
	elif cond(choice == "persuasion_fail"):
		if Rakugo.store.get("bard_dead"):
			jump("", "", "where_night_persuasion")
		else:
			ch.name = "Bard (Ghost)"
			jump("", "", "where_night_persuasion_fail")
	elif cond(choice == "insight"):
		jump("", "", "where_night_insight")
	else:
		jump("", "", "about_where")
	end_event()

func where_night_persuasion_fail():
	start_event("where_night_persuasion_fail")
	step()
	audioPlayer.play_sound("bard_persuasion_fail_0")
	say("bard", "Well, there's no need to be rude about it! Your constant questioning is really getting on my nerves. I ought to show you what that feels like...")
	step()
	ch.name = "Bard (Ghost)"
	audioPlayer.play_sound("bard_death")
	say(null, "Before the bard could demonstrate what she meant by that, she was drowned out by a loud power chord coming from her lute. It rose up and clubbed her in the head. She fell to the ground, and a quick check of her vitals revealed she was not just unconscious. A few moments later, she rose as a ghost.")
	step()
	audioPlayer.play_sound("bard_persuasion_fail_1")
	say("bard", "I suppose I may as well answer your questions.")
	step()
	jump("", "", "where_night_persuasion")
	end_event()

func where_night_persuasion():
	start_event("where_night_persuasion")
	step()
	audioPlayer.play_sound("bard_where_night_persuasion_0")
	say("bard", "Now that you mention it, I think I heard some animals. There was squeaking, so probably rats? Also there was an owl hooting. I think that came from outside. That all seemed pretty normal, so I didn’t think it was worth mentioning.")
	step()
	jump("", "", "where_night_menu")
	end_event()

func where_night_insight():
	start_event("where_night_insight")
	step()
	audioPlayer.play_sound("bard_where_night_insight_0")
	say("bard", "I suppose I wasn’t there quite the whole night. I popped out into the hallway briefly, just cause I was curious if there was anything going on.")
	step()
	audioPlayer.play_sound("bard_where_night_insight_1")
	say("bard", "Did I see anything? Oh, yes, the rogue was there for a bit. It looked like she was brandishing her sword at the floorboards. Kind of weird.")
	step()
	jump("", "", "where_night_menu")
	end_event()

func where_amulet():
	start_event("where_amulet")
	Tavern.change_image("Treasure")
	step()
	audioPlayer.play_sound("bard_where_amulet_0")
	say("bard", "I was near the back, right behind the wizard. To be honest, I wasn’t really paying much attention: a lot of stuff goes on up there that doesn’t seem to mean much in the end.")
	step()
	audioPlayer.play_sound("bard_where_amulet_1")
	say("bard", "But I saw the wizard’s eyes light up a bit. She told me it sounded like the cleric has discovered something and then stowed it real fast, so the wizard thought it might be some kind of magical item. That got my attention.")
	step()
	audioPlayer.play_sound("bard_where_amulet_2")
	say("bard", "Magical items make good stories. Heck, it’s making a good story right now! I mean, maybe not the happiest one...")
	step()
	jump("", "", "about_where")
	end_event()

func about_amulet():
	start_event("about_amulet")
	Tavern.change_image("Amulet")
	audioPlayer.play_sound("bard_amulet")
	say("bard", "Ah yes, that pretty thing. What do you want to know about it?")
	var choice = menu([
		["What do you know about the amulet?", "know", {}],
		["What would you do if you had the amulet?", "motive", {}],
		["What was it like when the amulet was discovered?", "discovered", {}],
		["Never mind.", "back", {}]
	])
	step()
	if cond(choice == "back"):
		jump("", "", "first_dialogue")
	elif cond(choice == "know"):
		jump("", "", "amulet_know")
	elif cond(choice == "motive"):
		jump("", "", "amulet_motive")
	elif cond(choice == "discovered"):
		jump("", "", "amulet_discovered")
	end_event()

func amulet_know():
	start_event("amulet_know")
	step()
	audioPlayer.play_sound("bard_amulet_know_0")
	say("bard", "So, I heard it was a protection amulet. I assume we’re talking protection from danger? I would think this would have to have some limit to its powers, but I sure don’t know what those limits are.")
	step()
	audioPlayer.play_sound("bard_amulet_know_1")
	say("bard", "It probably doesn’t have unlimited range: those kinda items are extremely rare. And it could affect one person or maybe multiple people, that’s possible too.")
	step()
	audioPlayer.play_sound("bard_amulet_know_2")
	say("bard", "But the real question is, how much protection is it gonna give you? And that’s what I definitely cannot answer.")
	step()
	jump("", "", "amulet_know_menu")
	end_event()

func amulet_know_menu():
	start_event("amulet_know_menu")
	step()
	if Settings.get("accessibility"):
		audioPlayer.play_sounds(["1", "bard_amulet_know_option_1", "2", "bard_amulet_know_option_2"])
	say(null, """1. Thank you, you’ve been very helpful. Anything else I should know about this amulet?
2. That’s a lot of detail! How do you know so much?""")
	var choice = menu([
		["1", "persuasion", {}],
		["2", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "persuasion"):
		jump("", "", "amulet_know_persuasion")
	elif cond(choice == "insight"):
		jump("", "", "amulet_know_insight")
	else:
		jump("", "", "about_amulet")
	end_event()

func amulet_know_persuasion():
	start_event("amulet_know_persuasion")
	step()
	audioPlayer.play_sound("bard_amulet_know_persuasion_0")
	say("bard", "I’ve shared all I know. I hope it’s helpful for you.")
	step()
	jump("", "", "amulet_know_menu")
	end_event()

func amulet_know_insight():
	start_event("amulet_know_insight")
	step()
	audioPlayer.play_sound("bard_amulet_know_insight_0")
	say("bard", "I’ve encountered items like this before. I almost got to keep the ring of protection we found in my last group: basically it made it a little harder for the wearer to get hurt.")
	step()
	audioPlayer.play_sound("bard_amulet_know_insight_1")
	say("bard", "But they weren’t fond of me, and the ring went to someone else. Rather disappointing.")
	step()
	jump("", "", "amulet_know_menu")
	end_event()

func amulet_motive():
	start_event("amulet_motive")
	step()
	audioPlayer.play_sound("bard_amulet_motive_0")
	say("bard", "Well I mean I know we have a strong team and generally our own skills are sufficient. But it certainly wouldn’t hurt to have a little more protection here. Think of it as... insurance.")
	step()
	audioPlayer.play_sound("bard_amulet_motive_1")
	say("bard", "Some of our members are a little more likely to go down in a fight. Especially that wizard. So I suppose if I had to pick one person, I’d go with her. She’s an invaluable asset to the team.")
	step()
	jump("", "", "amulet_motive_menu")
	end_event()

func amulet_motive_menu():
	start_event("amulet_motive_menu")
	step()
	if Settings.get("accessibility"):
		audioPlayer.play_sounds(["1", "bard_amulet_motive_option_1"])
	say(null, """1. Are you sure that’s who you’d use it for?""")
	var choice = menu([
		["1", "insight", {}],
		["Done", "done", {}]
	])
	step()
	if cond(choice == "insight"):
		jump("", "", "amulet_motive_insight")
	else:
		jump("", "", "about_amulet")
	end_event()

func amulet_motive_insight():
	start_event("amulet_motive_insight")
	step()
	audioPlayer.play_sound("bard_amulet_motive_insight_0")
	say("bard", "Yes, the wizard is very important to me, so don’t you dare go hurting her.")
	step()
	jump("", "", "amulet_motive_menu")
	end_event()

func amulet_discovered():
	start_event("amulet_discovered")
	step()
	audioPlayer.play_sound("bard_amulet_discovered_0")
	say("bard", "I must say, it was quite the commotion. The cleric put that amulet away and suddenly everyone was whispering amongst themselves wondering just what that shiny thing could be. It took the paladin’s threats to get them all to shut up.")
	step()
	jump("", "", "about_amulet")
	end_event()
